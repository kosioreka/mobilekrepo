﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Mobilek.Helper_Classes;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using System.IO;
using GalaSoft.MvvmLight.Threading;
using System.Collections;
using System.Windows.Documents;
using GalaSoft.MvvmLight;
using Mobilek.DTO;

namespace Mobilek
{
    /// <summary>
    /// The <c>UserPanelViewModel</c> defines user's panel
    /// <c>UserPanelViewModel</c> has following tab views: order history, profile, new order
    /// </summary>
    class UserPanelViewModel : ObservableObject, IPageViewModel
    {
        #region private fields
        static bool isRegistered = false;
        private ICommand _toLogOut;
        private ICommand _chooseCars;
        private ICommand _editProfile;
        private ICommand _chooseStartStation;
        private ICommand _chooseEndStation;
        private ICommand _submitOrder;

        private CustomerDTO _theCustomer;
        private string _logInInfo;
        private string _status;
        private StationDTO _startingStation;
        private StationDTO _endingStation;
#pragma warning disable CS0414 // The field 'UserPanelViewModel._submitedOrder' is assigned but its value is never used
        private bool _submitedOrder;
#pragma warning restore CS0414 // The field 'UserPanelViewModel._submitedOrder' is assigned but its value is never used
        private Collection<CarDTO> _selectedCars;
        private List<CarDTO> _carsFromOrder;
        private OrderDTO _selectedOrder;

        ObservableCollection<OrderDTO> _ordersCollection = new ObservableCollection<OrderDTO>();
        Collection<StationDTO> _startingStationCollection = new Collection<StationDTO>();
        Collection<StationDTO> _endingStationCollection = new Collection<StationDTO>();
        #endregion

        #region properties
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public StationDTO StartingStation
        {
            get { return _startingStation; }
            set
            {                
                _startingStation = value;
                OnPropertyChanged("StartingStation");
                StartingStationCollection = new Collection<StationDTO>();
                StartingStationCollection.Add(_startingStation);
            }
        }
        public StationDTO EndingStation
        {
            get { return _endingStation; }
            set
            {
                _endingStation = value;
                OnPropertyChanged("EndingStation");
                EndingStationCollection = new Collection<StationDTO>();
                EndingStationCollection.Add(_endingStation);
            }
        }
        public OrderDTO SelectedOrder
        {
            get { return _selectedOrder; }
            set
            {
                _selectedOrder = value;
                OnPropertyChanged("SelectedOrder");
                //using (var entities = new MobilekEntities())
                {
                    if (_selectedOrder != null)
                    {
                        //var service = new Mobilek.Services.OrderDTOService();
                        //var order =await service.Get(_selectedOrder.Id);
                        //Order order = entities.Orders.FirstOrDefault<Order>(o => o.Id == _selectedOrder.Id);
                        //CarsFromOrder = order.Cars.ToList();
                        GetOrder(_selectedOrder.Id);
                    }
                }
            }
        }
        private async void  GetOrder(int id)
        {
            var service = new Mobilek.Services.OrderDTOService(TheCustomer.userName,TheCustomer.password);
            var order = await service.Get(_selectedOrder.Id);
            CarsFromOrder = order.Cars.ToList();
        }

        public List<CarDTO> CarsFromOrder
        {
            get { return _carsFromOrder; }
            set
            {
                _carsFromOrder = value;
                OnPropertyChanged("CarsFromOrder");
            }
        }
        public UserPanelViewModel(CustomerDTO customer)
        {
            TheCustomer = customer;
            GetOrders();
            ReceiveCollectionInfo();
            StartTime = DateTime.Today;
            EndTime = DateTime.Today;
        }
        public Collection<CarDTO> SelectedCars
        {
            get { return _selectedCars; }
            set
            {
                _selectedCars = value;
                OnPropertyChanged("SelectedCars");
            }
        }
        public Collection<StationDTO> StartingStationCollection
        {
            get
            {
                return _startingStationCollection;
            }
            set
            {
                _startingStationCollection = value;
                OnPropertyChanged("StartingStationCollection");
            }
        }
        public Collection<StationDTO> EndingStationCollection
        {
            get
            {
                return _endingStationCollection;
            }
            set
            {
                _endingStationCollection = value;
                OnPropertyChanged("EndingStationCollection");
            }
        }
        public ObservableCollection<OrderDTO> OrdersCollection
        {
            get
            {
                return _ordersCollection;
            }
            set
            {
                _ordersCollection = value;
                OnPropertyChanged("OrdersCollection");
            }
        }
        public string Name
        {
            get { return "Your Orders History"; }
        }
        public String LogInInfo
        {
            get { return _logInInfo; }
            set
            {
                _logInInfo = value;
                OnPropertyChanged("logInInfo");
            }
        }
        public CustomerDTO TheCustomer
        {
            get { return _theCustomer; }
            set
            {
                _theCustomer = value;
                if (_theCustomer != null && _theCustomer.userName != null)
                {
                    _logInInfo = _theCustomer.userName.ToString();
                }
                OnPropertyChanged("TheCustomer");
            }
        }
        public string Status
        {
            get { return _status; }
            set
            {
                if (value == "true") { _status = "finished"; }
                else if (value == "false") { _status = "ongoing"; }
                else { _status = value; }
                OnPropertyChanged("Status");
            }
        }
        #endregion

        #region methods
        private void ReceiveCollectionInfo()
        {
            if (!isRegistered)
            {
                Messenger.Default.Register<MessageCommunicatorNewOrder>(this, (mc) =>
                {
                    if (mc.carsCollection != null)
                        this.SelectedCars = mc.carsCollection;
                    if (mc.startingStation != null)
                    {
                        if (this.StartingStation != null)
                            if (!this.StartingStation.Id.Equals(mc.startingStation.Id))
                                SelectedCars = null;
                        this.StartingStation = mc.startingStation;
                    }
                    if (mc.endingStation != null)
                        this.EndingStation = mc.endingStation;
                    if (mc.submittedOrder == true)
                    {
                        _submitedOrder = true;
                        MakeOrder();
                    }
                    if (mc.customer != null)
                    {
                        TheCustomer = mc.customer;
                    }
                });
                Messenger.Default.Register<MessageCommunicatorProfileEdit>(this, (mcpe) =>
                {
                    TheCustomer = mcpe.customer;
                });
                isRegistered = true;
            }
            


        }

        private async void GetOrders()
        {
            //using (var entities = new MobilekEntities())
            {
                var service = new Mobilek.Services.OrderDTOService(TheCustomer.userName,TheCustomer.password);
                var orders = await service.Get();
                var orderList = orders.Where(o => o.Customer.Id.Equals(_theCustomer.Id));
                //var orderList = entities.Orders.AsEnumerable().Where(o => o.Customer.Id.Equals(_theCustomer.Id));
                OrdersCollection = ToObservableCollectioncs.ToObservableCollection<OrderDTO>(orderList);
            }
        }
        private async void MakeOrder()
        {
            //using (var entities = new MobilekEntities())
            {
                OrderDTO order = new OrderDTO()
                {
                    customerId = TheCustomer.Id,
                    startDate = StartTime,
                    endDate = EndTime,
                    returnStationId = EndingStation.Id,
                    Cars = SelectedCars
                };
                var service =new Services.WorkerDTOService(TheCustomer.userName,TheCustomer.password);
                var wor = await service.Get();
                var worker = wor.FirstOrDefault();
                if (worker!=null)
                {
                    order.workerId = worker.id;
                    //entities.Orders.Add(order);
                    //entities.SaveChanges();
                    var orderService = new Services.OrderDTOService(TheCustomer.userName, TheCustomer.password);
                    await orderService.Post(order);
                }
            }
            GetOrders();
        }
        #endregion

        #region Commands
        public ICommand EditProfile
        {
            get
            {
                if (_editProfile == null)
                {
                    _editProfile = new RelayCommand(
                        param => GoToEditProfileDlg());

                }
                return _editProfile;
            }
        }

        public ICommand ChooseStartStation
        {
            get
            {
                if (_chooseStartStation == null)
                {
                    _chooseStartStation = new RelayCommand(
                        param => GoToStationDialog("StartStation"));

                }
                return _chooseStartStation;
            }
        }
        public ICommand ChooseEndStation
        {
            get
            {
                if (_chooseEndStation == null)
                {
                    _chooseEndStation = new RelayCommand(
                        param => GoToStationDialog("EndStation"));

                }
                return _chooseEndStation;
            }
        }
        public ICommand ChooseCars
        {
            get
            {
                if (_chooseCars == null)
                {
                    _chooseCars = new RelayCommand(
                        param => GoToCarsDialog());

                }
                return _chooseCars;
            }
        }
        public ICommand SubmitOrder
        {
            get
            {
                if (_submitOrder == null)
                {
                    _submitOrder = new RelayCommand(
                        param => GoToSubmitOrderDialog());
                }
                return _submitOrder;
            }
        }
        public ICommand ToLogOut
        {
            get
            {
                if (_toLogOut == null)
                {
                    _toLogOut = new RelayCommand(
                        param => ToWelcomeView()
                    );
                }
                return _toLogOut;
            }
        }
        #endregion

        #region Navigation
        private async void GoToCarsDialog()
        {
            if (StartingStation != null)
            {
                CustomDialog d = new CustomDialog();
                d.Content = new CarsOrderView(StartingStation,TheCustomer);
                var metro = App.Current.MainWindow as MetroWindow;
                await metro.ShowMetroDialogAsync(d);
            }
            else
            {
                //
            }
        }
        private async void GoToSubmitOrderDialog()
        {
            if (StartingStation != null && EndingStation != null && SelectedCars != null)//to implement
            {
                CustomDialog d = new CustomDialog();
                d.Content = new SubmitOrderView();
                var metro = App.Current.MainWindow as MetroWindow;
                await metro.ShowMetroDialogAsync(d);
            }
            else
            {
                //
            }
        }
        private async void GoToStationDialog(string stationType)
        {

            CustomDialog d = new CustomDialog();
            d.Content = new StationOrderView(stationType, _theCustomer);
            var metro = App.Current.MainWindow as MetroWindow;
            await metro.ShowMetroDialogAsync(d);

        }
        private async void GoToEditProfileDlg()
        {
            CustomDialog d = new CustomDialog();
            d.Content = new ProfileEditView(_theCustomer);
            var metro = App.Current.MainWindow as MetroWindow;
            await metro.ShowMetroDialogAsync(d);
        }


        private void ToWelcomeView()
        {
            var msg = new ChangePageHelper() { PageName = new WelcomeViewModel() };
            Messenger.Default.Send<ChangePageHelper>(msg);
        }


        #endregion
    }
}
