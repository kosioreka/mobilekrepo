﻿
//using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using System.Linq;
//using Mobilek.Helper_Classes;
//using Mobilek.Domain;

//namespace Mobilek.Tests
//{
//    [TestClass]
//    public class ValidationTestes
//    {
//        [TestMethod]
//        public void WorkerLoginValidationTest()
//        {
//            WorkerLoginViewModel vm = new WorkerLoginViewModel();
//            vm.Login_UserName = "";
//            vm.Validate();
//            Assert.IsTrue(vm.validationErrors.ContainsKey("Login_UserName"));
//            Assert.IsTrue(vm.validationErrors.ContainsValue("Please enter your user name"));

//            vm.Login_UserName = "!@#$%^&*()";
//            vm.Validate();
//            Assert.IsTrue(vm.validationErrors.ContainsKey("Login_UserName"));
//            Assert.IsTrue(vm.validationErrors.ContainsValue("User not found"));

//            using (var entities = new MobilekEntities())
//            {
//                var user = entities.Workers.FirstOrDefault();
//                if (user != null)
//                {
//                    vm.Login_UserName = user.login;
//                    vm.Login_Password = user.password;
//                    vm.Validate();
//                    Assert.IsTrue(vm.validationErrors.Count == 0);
//                    vm.Login_Password = "BAD";
//                    vm.Validate();
//                    Assert.IsTrue(vm.validationErrors.ContainsKey("Login_UserName"));
//                    Assert.IsTrue(vm.validationErrors.ContainsValue("Incorrect password"));
//                }
//            }
//        }
//        [TestMethod]
//        public void UserLoginValidationTest()
//        {
//            var vm = new UserLoginViewModel();
//            vm.Login_UserName = "";
//            vm.ValidateLogin();
//            Assert.IsTrue(vm.validationErrors.ContainsKey("Login_UserName"));
//            Assert.IsTrue(vm.validationErrors.ContainsValue("Please enter your user name"));

//            vm.Login_UserName = "!@#$%^&*()";
//            vm.ValidateLogin();
//            Assert.IsTrue(vm.validationErrors.ContainsKey("Login_UserName"));
//            Assert.IsTrue(vm.validationErrors.ContainsValue("User not found"));


//        }
//        [TestMethod]
//        public void UserRegistryValidationTest()
//        {
//            var vm = new UserLoginViewModel();
//            vm.FirstName = "";
//            vm.ValidateRegistry();
//            Assert.IsTrue(vm.validationErrors.ContainsKey("FirstName"));
//            Assert.IsTrue(vm.validationErrors.ContainsValue("First name is required!"));
//            vm.FirstName = "   ";
//            vm.ValidateRegistry();
//            Assert.IsTrue(vm.validationErrors.ContainsKey("FirstName"));
//            Assert.IsTrue(vm.validationErrors.ContainsValue("First name is required!"));

//            vm.Surname = "   ";
//            vm.ValidateRegistry();
//            Assert.IsTrue(vm.validationErrors.ContainsKey("Surname"));
//            Assert.IsTrue(vm.validationErrors.ContainsValue("Surname is required!"));

//            vm.Pesel = "";
//            vm.ValidateRegistry();
//            Assert.IsTrue(vm.validationErrors.ContainsKey("Pesel"));
//            Assert.IsTrue(vm.validationErrors.ContainsValue("Pesel is required!"));

//            vm.Pesel = "ABC";
//            vm.ValidateRegistry();
//            Assert.IsTrue(vm.validationErrors.ContainsKey("Pesel"));
//            Assert.IsTrue(vm.validationErrors.ContainsValue("Only digits are allowed in Pesel field."));

//            vm.Pesel = "94051611931";
//            vm.ValidateRegistry();
//            Assert.IsTrue(!vm.validationErrors.ContainsKey("Pesel"));

//            vm.Pesel = "12312312312321321";
//            vm.ValidateRegistry();
//            Assert.IsTrue(vm.validationErrors.ContainsKey("Pesel"));
//            Assert.IsTrue(vm.validationErrors.ContainsValue("Wrong Pesel Number!"));

//            vm.UserName = "";
//            vm.ValidateRegistry();
//            Assert.IsTrue(vm.validationErrors.ContainsKey("UserName"));
//            Assert.IsTrue(vm.validationErrors.ContainsValue("User name is required!"));

//            using (var entities = new MobilekEntities())
//            {
//                var user = entities.Customers.FirstOrDefault();
//                if (user != null)
//                {
//                    vm.UserName = user.userName;
//                    vm.ValidateRegistry();
//                    Assert.IsTrue(vm.validationErrors.ContainsKey("UserName"));
//                    Assert.IsTrue(vm.validationErrors.ContainsValue("User name is not available. Please choose different one"));
//                }
//            }

//            vm.PhoneNr = "";
//            vm.ValidateRegistry();
//            Assert.IsTrue(vm.validationErrors.ContainsKey("PhoneNr"));
//            Assert.IsTrue(vm.validationErrors.ContainsValue("Phone is required!"));

//            vm.PhoneNr = "ABC";
//            vm.ValidateRegistry();
//            Assert.IsTrue(vm.validationErrors.ContainsKey("PhoneNr"));
//            Assert.IsTrue(vm.validationErrors.ContainsValue("Only digits are allowed in Phone Number field."));

//            vm.Email = "";
//            vm.ValidateRegistry();
//            Assert.IsTrue(vm.validationErrors.ContainsKey("Email"));
//            Assert.IsTrue(vm.validationErrors.ContainsValue("Email is required!"));

//            vm.Email = "mailmailmailmail@mailmailmail.pl";
//            vm.ValidateRegistry();
//            Assert.IsTrue(!vm.validationErrors.ContainsKey("Email"));
//            Assert.IsTrue(!vm.validationErrors.ContainsValue("Email is required!"));

//            vm.Password = "";
//            vm.ValidateRegistry();
//            Assert.IsTrue(vm.validationErrors.ContainsKey("UserName"));

//            vm.Password = "123";
//            vm.ValidateRegistry();
//            Assert.IsTrue(vm.validationErrors.ContainsKey("UserName"));




//        }
//    }
//}
