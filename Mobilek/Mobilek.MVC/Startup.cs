﻿using System.Net;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Mobilek.MVC.Startup))]
namespace Mobilek.MVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ServicePointManager
                .ServerCertificateValidationCallback +=
                    (sender, cert, chain, sslPolicyErrors) => true;
            ConfigureAuth(app);

        }
    }
}
