﻿$(function () {

    $("#stationGrid").jqGrid({
        url: "/Worker/GetStations",
        datatype: 'json',
        mtype: 'Get',
        colNames: ['Id', 'City', 'Street', 'Street No', 'Zip code'],
        colModel: [
            { key: true, hidden: true, name: 'Id', index: 'Id', editable: false, searchoptions: { sopt: ['eq', 'ne', 'cn'] } },
            { key: false, name: 'city', index: 'city', editable: true, edittype: "text", editrules: { custom: true, custom_func: textBoxValidation }, searchoptions: { sopt: ['eq', 'ne', 'cn'] } },
            { key: false, name: 'street', index: 'street', editable: true, edittype: "text", editrules: { custom: true, custom_func: textBoxValidation }, searchoptions: { sopt: ['eq', 'ne', 'cn'] } },
            { key: false, name: 'streetNumer', index: 'streetNumer', editable: true, edittype: "text", editrules: { custom: true, custom_func: numericBoxValidation }, searchoptions: { sopt: ['eq', 'ne', 'cn'] } },
            { key: false, name: 'zipCode', index: 'zipCode', editable: true, edittype: "text", editrules: { custom: true, custom_func: zipCodeValidation }, searchoptions: { sopt: ['eq', 'ne', 'cn'] } }],
        pager: jQuery('#pagerStation'),
        rowNum: 10,
        rowList: [10, 20, 30, 40],
        height: '100%',
        viewrecords: true,
        loadonce:true,
        caption: 'Stations',
        emptyrecords: 'No records to display',
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
            Id: "0"
        },
        autowidth: true,
        multiselect: false
    }).navGrid('#pagerStation', { edit: true, add: true, del: true, search: true, refresh: true },
        {
            // edit options
            zIndex: 100,
            url: '/Worker/EditStation',
            closeOnEscape: true,
            closeAfterEdit: true,
            recreateForm: true,
            afterComplete: function (response) {
                if (response.responseText) {
                    alert(response.responseText);
                    $.ajax({
                        url: '/Worker/GetStationCars',
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var respons = data; //jQuery.parseJSON(data);
                            var s = '<select>';
                            if (respons && respons.length) {
                                for (var i = 0, l = respons.length; i < l ; i++) {
                                    var ri = respons[i];
                                    s += '<option value="' + ri + '">' + ri + '</option>';
                                }
                            }
                            //return s + "</select>";
                            $("#stationName").html(s);
                            $("#stationGrid").jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                            //return [true, ''];
                            
                        },
                        error: function () {
                        }
                    });
                }
            }
        },
        {
            // add options
            zIndex: 100,
            url: "/Worker/CreateStation",
            closeOnEscape: true,
            closeAfterAdd: true,
            afterComplete: function (response) {
                if (response.responseText) {
                    alert(response.responseText);
                    $.ajax({
                        url: '/Worker/GetStationCars',
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var response = data;//jQuery.parseJSON(data);
                            var s = '<select>';
                            if (response && response.length) {
                                for (var i = 0, l = response.length; i < l ; i++) {
                                    var ri = response[i];
                                    s += '<option value="' + ri + '">' + ri + '</option>';
                                }
                            }
                            //return s + "</select>";
                            $("#stationName").html(s);
                            $("#stationGrid").jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');

                        },
                        error: function () {
                        }
                    });
                }



            }
        },
        {
            // delete options
            zIndex: 100,
            url: "/Worker/DeleteStation",
            closeOnEscape: true,
            closeAfterDelete: true,
            recreateForm: true,
            msg: "Are you sure you want to delete this task?",
            afterComplete: function (response) {
                if (response.responseText) {
                    alert(response.responseText);
                    $("#stationGrid").jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');

                }
            }
        },
        {
            closeOnEscape: true,
            multipleSearch: true,
            closeAfterSearch: true
        }


        );
    function textBoxValidation(value) {
        if (value == "") {
            return [false, "Field cannot be empty"];
        }
        return [true];
    }
    function numericBoxValidation(value) {
        if (value == "") {
            return [false, "Field cannot be empty"];
        }
        else if (isNaN(value)) {
            return [false, "Must be a number."];
        }
        return [true];
    }
    function zipCodeValidation(value) {
        if (value == "") {
            return [false, "Field cannot be empty"];
        }
        var re = /^\d{5}$/;
        if (!re.test(value)) {
            return [false, "Incorret Zip Code format."];
        }
        return [true];
    }
});
