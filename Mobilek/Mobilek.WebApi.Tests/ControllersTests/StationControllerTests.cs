﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Mobilek.DTO;
using Mobilek.WebApi.Controllers;
using System.Web.Http;
using System.Web.Http.Results;
using Mobilek.Domain;
using Mobilek.Services;
using AutoMapper;

namespace Mobilek.WebApi.Tests
{
    [TestClass]
    public class StationControllerTests
    {
        [TestInitialize]
        public void testInit()
        {
            Mobilek.WebApi.AutomapperConfigure.Configure();
        }
        [TestMethod]
        public void GetAllStations_ShouldReturnAllStationsTest()
        {
            StationDTO[] stations = new StationDTO[5];

            var mockRepository = new Mock<IStationRepository>();
            mockRepository.Setup(x => x.GetAll()).Returns(new StationDTO[stations.Count()]);
            var controller = new StationController(mockRepository.Object);

            var result = controller.Get() as OkNegotiatedContentResult<IEnumerable<StationDTO>>;
            Assert.AreEqual(stations.Count(), result.Content.Count());
        }
        //working
        [TestMethod]
        public void GetReturnsStationsWithSameIdTest()
        {
            int id = 5;
            int noId = 10;

            var mockRepository = new Mock<IStationRepository>();
            mockRepository.Setup(x => x.Find(id)).Returns(new StationDTO { Id = id });

            var controller = new StationController(mockRepository.Object);
         
            IHttpActionResult actionResult = controller.Get(id);
            var contentResult = actionResult as OkNegotiatedContentResult<StationDTO>;
          
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(id, contentResult.Content.Id);

            var noActionResult = controller.Get(noId);

            Assert.IsInstanceOfType(noActionResult, typeof(NotFoundResult));
        }
        [TestMethod]
        public void GetReturnsStationsByCityTest()
        {
            string city = "city";
            string noCity = "noCity";

            // Arrange
            var mockRepository = new Mock<IStationRepository>();
            mockRepository.Setup(x => x.FindByCity(city)).Returns(new List<StationDTO>() { new StationDTO() });
            var controller = new StationController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Get(city);
            var createdResult = actionResult as OkNegotiatedContentResult<IEnumerable<StationDTO>>;
            var createdResult2 = actionResult as CreatedAtRouteNegotiatedContentResult<IEnumerable<StationDTO>>;

            // Assert
            Assert.IsNotNull(createdResult.Content);
            // Assert
            Assert.IsNotNull(actionResult);

            IHttpActionResult noActionResult = controller.Get(noCity);
            Assert.IsInstanceOfType(noActionResult, typeof(NotFoundResult));
        }
    }
}
