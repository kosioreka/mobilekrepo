﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Mobilek.DTO;
using Mobilek.WebApi.Controllers;
using System.Web.Http;
using System.Web.Http.Results;
using Mobilek.Domain;
using AutoMapper;

namespace Mobilek.WebApi.Tests
{
    [TestClass]
    public class CustomerControllerTest
    {
        [TestInitialize]
        public void testInit()
        {
            Mobilek.WebApi.AutomapperConfigure.Configure();
        }
        [TestMethod]
        public void GetAllCustomers_Test()
        {

            var mockRepo = new Mock<ICustomerRepository>();
            mockRepo.Setup(s => s.GetAll()).Returns(new List<CustomerDTO>() { new CustomerDTO() });

            var controller = new CustomerController(mockRepo.Object);

            var getallResult = controller.Get();

            Assert.IsInstanceOfType(getallResult, typeof(OkNegotiatedContentResult<IEnumerable<CustomerDTO>>));

            var or = getallResult as OkNegotiatedContentResult<IEnumerable<CustomerDTO>>;
            Assert.IsTrue(or.Content.Count() == 1);

        }
        [TestMethod]
        public void FindCustomer_Test()
        {

            var mockRepo = new Mock<ICustomerRepository>();
            int id = 5;
            int noid = 33;

            mockRepo.Setup(s => s.Find("5")).Returns(new CustomerDTO() { Id = id });

            var controller = new CustomerController(mockRepo.Object);

            var getallResult = controller.Get(id.ToString());

            Assert.IsInstanceOfType(getallResult, typeof(OkNegotiatedContentResult<CustomerDTO>));

            var worker = getallResult as OkNegotiatedContentResult<CustomerDTO>;

            Assert.IsTrue(worker.Content.Id == id);

            var getresults2 = controller.Get(noid.ToString());

        }
    }
}
