﻿$(function () {
    $("#orderStationGrid").jqGrid({
        url: "/UserAccount/GetStations",
        datatype: 'json',
        mtype: 'Get',
        colNames: ['Id', 'City', 'Street', 'Street No', 'Zip code'],
        colModel: [
            { key: true, hidden: true, name: 'Id', index: 'Id', editable: false },
            { key: false, name: 'city', index: 'city', editable: false },
            { key: false, name: 'street', index: 'street', editable: false },
            { key: false, name: 'streetNumer', index: 'streetNumer', editable: false },
            { key: false, name: 'zipCode', index: 'zipCode', editable: false }],
        pager: jQuery('#pagerStation'),
        rowNum: 10,
        rowList: [10, 20, 30, 40],
        height: '100%',
        viewrecords: true,
        onSelectRow: function (id) {
            var rid = jQuery('#orderStationGrid').jqGrid("getGridParam", "selrow");
            if (rid) {
                var row = jQuery('#orderStationGrid').jqGrid("getRowData", rid);
                alert(row.city + ", " + row.street + ", " + row.streetNumer + ", " + row.zipCode);
            }
        },
        caption: 'Stations',
        emptyrecords: 'No records to display',
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
            Id: "0"
        },
        autowidth: true,
        multiselect: false
    }).navGrid('#pagerStation', { edit: false, add:false, del: false, search: true, refresh: true, })
});