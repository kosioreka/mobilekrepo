﻿using Mobilek.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Mobilek.Services
{
    public interface ICustomerDTOService
    {
        Task<CustomerDTO> Get(string id);

        Task<IList<CustomerDTO>> Get();

        Task<bool> Post(CustomerDTO item);

        Task<bool> Put(CustomerDTO item);

    }

    public class CustomerDTOService : WebApiService,ICustomerDTOService
    {
        public CustomerDTOService(string l,string p) : base("Customer")
        {
            this._httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue(
                    "Basic",
                    Convert.ToBase64String(
                        System.Text.ASCIIEncoding.ASCII.GetBytes(
                            string.Format("{0}:{1}", l, p))));

        }

        public async Task<CustomerDTO> Get(string id)
        {
            return await this.Get<CustomerDTO>(id);
        }
        public async Task<IList<CustomerDTO>> Get()
        {
            return await this.Get<IList<CustomerDTO>>("");
        }
        public async Task<bool> Post(CustomerDTO item)
        {
            return await this.Post<CustomerDTO, bool>("POST", item);
        }
        public async Task<bool> Put(CustomerDTO item)
        {
            return await this.Put<CustomerDTO, bool>("PUT", item);
        }
    }
}
