﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Mobilek.DTO;
using Mobilek.Domain;

namespace Mobilek.WebApi
{
    public class AutomapperConfigure
    {
        public static void Configure()
        {
            Mapper.CreateMap<StationDTO, Station>();
            //    .ForMember(dest => dest.Cars, opt => opt.MapFrom(s => Mapper.Map<ICollection<CarDTO>, ICollection<Car>>(s.Cars)))
            //    .ForMember(dest => dest.Orders, opt => opt.MapFrom(s => Mapper.Map<ICollection<OrderDTO>, ICollection<Order>>(s.Orders)));
            Mapper.CreateMap<Station, StationDTO>();
            //    .ForMember(dest => dest.Cars, opt => opt.MapFrom(s => Mapper.Map<ICollection<Car>, ICollection<CarDTO>>(s.Cars)))
            //    .ForMember(dest => dest.Orders, opt => opt.MapFrom(s => Mapper.Map<ICollection<Order>, ICollection<OrderDTO>>(s.Orders)));

            Mapper.CreateMap<Customer, CustomerDTO>();
            Mapper.CreateMap<CustomerDTO, Customer>();

            Mapper.CreateMap<Car, CarDTO>()
                .ForMember(dest => dest.stationName, opt => opt.MapFrom(s => s.Station.city + " " + s.Station.street));
            //        .ForMember(dest => dest.Station, opt => opt.MapFrom(s => Mapper.Map<Station, StationDTO>(s.Station)));
            Mapper.CreateMap<CarDTO, Car>();
            //    .ForMember(dest => dest.Orders, opt => opt.MapFrom(s => Mapper.Map<ICollection<OrderDTO>, ICollection<Order>>(s.Orders)))
            //    .ForMember(dest => dest.Station, opt => opt.MapFrom(s => Mapper.Map<StationDTO, Station>(s.Station)));


            

            Mapper.CreateMap<Worker, WorkerDTO>();
            Mapper.CreateMap<WorkerDTO, Worker>();


            Mapper.CreateMap<Order, OrderDTO>()
                .ForMember(dest => dest.Cars, opt => opt.MapFrom(s => Mapper.Map<ICollection<Car>, ICollection<CarDTO>>(s.Cars)))
                .ForMember(dest => dest.Station, opt => opt.MapFrom(s => Mapper.Map<Station, StationDTO>(s.Station)))
                .ForMember(dest => dest.Worker, opt => opt.MapFrom(s => Mapper.Map<Worker, WorkerDTO>(s.Worker)))
                .ForMember(dest => dest.Customer, opt => opt.MapFrom(s => Mapper.Map<Customer, CustomerDTO>(s.Customer)));
            Mapper.CreateMap<OrderDTO, Order>()
                .ForMember(dest => dest.Cars, opt => opt.MapFrom(s => Mapper.Map<ICollection<CarDTO>, ICollection<Car>>(s.Cars)))
                .ForMember(dest => dest.Station, opt => opt.MapFrom(s => Mapper.Map<StationDTO, Station>(s.Station)))
                .ForMember(dest => dest.Worker, opt => opt.MapFrom(s => Mapper.Map<WorkerDTO, Worker>(s.Worker)))
                .ForMember(dest => dest.Customer, opt => opt.MapFrom(s => Mapper.Map<CustomerDTO, Customer>(s.Customer)));


            
        }
    }
}