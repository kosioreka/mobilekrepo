﻿using Mobilek.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Mobilek.MVC.Models
{
    public class OrderModel
    {
        public OrderDTO order { get; set; }
        public List<CarDTO> cars { get; set; }
    }
    public class MakeOrderModel
    {
        [Required]
        [Display(Name="Starting Date")]
        public DateTime startingDate { get; set; }
        [Required]      
        [Display(Name = "Ending Date")]       
        public DateTime endingDate { get; set; }
        //public List<CarDTO> cars { get; set; }
        //public List<SelectListItem> carsItems { get; set; }
        [Required]
        [Display(Name="Station")]
        public int stationItem { get; set; }
        [Required]
        [MinLength(1)]
        [Display(Name="Cars")]
        public int[] carItems { get; set; }

    }
    public class StationModel
    {
        public int Id { get; set; }
        public string street { get; set; }
        public int streetNumer { get; set; }
        public string city { get; set; }
        public string zipCode { get; set; }

        public override string ToString()
        {
            string strOut = string.Format("{0} {1}", street, streetNumer);
            return strOut;
        }

    }
}