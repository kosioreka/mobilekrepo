﻿using GalaSoft.MvvmLight.Messaging;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Mobilek.DTO;
using Mobilek.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;


namespace Mobilek
{
    /// <summary>
    /// The <c>StationEditViewModel</c> defines view for worker's editing stations properties functinonaltiy
    /// </summary>
    /// <remarks>
    /// The <c>StationEditViewModel</c> requires a station object, that will be modified
    /// </remarks>
    public class StationEditViewModel : ObservableObject, IPageViewModel
    {
        #region private fields
        private string city;
        private string street;
        private string zipCode;
        private string streetNo;
        private StationDTO selected;
        private ICommand acceptCommand;
        private ICommand cancelCommand;
        private WorkerDTO w;
        #endregion

        #region properties
        public string City
        {
            get { return city; }
            set
            {
                city = value;
                OnPropertyChanged("City");
            }
        }
        public string Street
        {
            get { return street; }
            set
            {
                street = value;
                OnPropertyChanged("Street");
            }
        }
        public string ZipCode
        {
            get { return zipCode; }
            set
            {
                zipCode = value;
                OnPropertyChanged("ZipCode");
            }
        }
        public string StreetNo
        {
            get { return streetNo; }
            set
            {
                streetNo = value;
                OnPropertyChanged("StreetNo");
            }
        }
        public string Name
        {
            get
            {
                return "Station Edit";
            }
        }
        #endregion

        #region commands
        public ICommand AcceptCommand
        {
            get
            {
                if (acceptCommand == null)
                {
                    acceptCommand = new RelayCommand(
                        param => Accept());

                }
                return acceptCommand;
            }
        }
        public ICommand CancelCommand
        {
            get
            {
                if (cancelCommand == null)
                {
                    cancelCommand = new RelayCommand(
                        param => Exit());

                }
                return cancelCommand;
            }
        }
        #endregion

        #region methods
        public StationEditViewModel(StationDTO e,WorkerDTO  w)
        {
            City = e.city;
            Street = e.street;
            ZipCode = e.zipCode;
            StreetNo = e.streetNumer.ToString();
            selected = e;
            this.w = w;
        }
        private async void Accept()
        {
            int no;
            if (Int32.TryParse(StreetNo, out no))
            {
                StationDTO toUpdate = new StationDTO()
                {
                    Id = selected.Id,
                    city = City,
                    street = Street,
                    zipCode = ZipCode,
                    streetNumer = no
                };
                var service = new StationDTOService(w.login,w.password);
                await service.Put(toUpdate);
                
            }

            var msg = new FireRefresh();
            Messenger.Default.Send<FireRefresh>(msg);
            Exit();
        }
        public void EntitiesEddit(int ID, string _city, string _street, string _zipCode, string _streetNo)
        {
            {
                var service = new Mobilek.Services.StationDTOService(w.login,w.password);
                var toEdit = service.Get(ID).Result;
                int no;
                if (Int32.TryParse(_streetNo, out no))
                {
                    toEdit.streetNumer = no;
                    toEdit.city = _city;
                    toEdit.street = _street;
                    toEdit.zipCode = _zipCode;
                }
                service.Post(toEdit);
            }
        }
        private async void Exit()
        {
            var metroWindow = App.Current.MainWindow as MetroWindow;
            var actualDialog = await metroWindow.GetCurrentDialogAsync<CustomDialog>();
            await metroWindow.HideMetroDialogAsync(actualDialog);
        }
        #endregion
    }
}
