﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Web;
using Mobilek.DTO;
using AutoMapper.QueryableExtensions;
using AutoMapper;
using Mobilek.Domain;


namespace Mobilek.WebApi
{
    public interface IWorkerRepository
    {
        IEnumerable<WorkerDTO> GetAll();
        WorkerDTO Find(string id);
        void Update(WorkerDTO item);
    }
    public class WorkerRepository : IWorkerRepository, IDisposable
    {
        private MobilekEntities entities = new MobilekEntities();


        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (entities != null)
                    entities.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public WorkerDTO Find(string id)
        {
            int o;
            if (int.TryParse(id, out o))
            {
                var worker = entities.Workers.FirstOrDefault(s => s.id == o);
                return Mapper.Map<WorkerDTO>(worker);
            }
            else
            {
                var worker = entities.Workers.FirstOrDefault(s => s.login == id);
                return Mapper.Map<WorkerDTO>(worker);
            }
        }

        public IEnumerable<WorkerDTO> GetAll()
        {
            return entities.Workers.ProjectTo<WorkerDTO>();
        }
        public void Update(WorkerDTO item)
        {
            var worker = entities.Workers.FirstOrDefault(s => s.id == item.id);
            if (worker != null)
            {

                worker.lastName = item.lastName;
                worker.firstName = item.firstName;
                worker.password = item.password;
                entities.SaveChanges();



            }
        }

    }
}