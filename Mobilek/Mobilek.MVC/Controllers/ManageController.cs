﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Mobilek.DTO;
using Mobilek.MVC.Constants;
using Mobilek.MVC.Helpers;
using Mobilek.MVC.ViewModels;
using Mobilek.Services;

namespace Mobilek.MVC.Controllers
{
    public class ManageController : Controller
    {
        private readonly ICustomerDTOService _customerDtoService ;
        private readonly IWorkerDTOService _workerDtoService;

        public ManageController(ICustomerDTOService customerDtoService,IWorkerDTOService workerDtoService)
        {
            _customerDtoService = customerDtoService;
            _workerDtoService = workerDtoService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ResetPasswordViewModel model)
        {
            var claims = ((ClaimsIdentity) User.Identity).Claims;
            var role = claims.FirstOrDefault(s => s.Type == ClaimTypes.Role);

            string msg;
            var result=AccountHelper.ValidateChangePassword(role, User.Identity.Name, model.OldPassword, model.NewPassword,
                _customerDtoService, _workerDtoService, out msg);

            if (result)
            {
                return RedirectToAction("Index", "Manage");
            }
            else
            {
                AddErrors(msg);
                return View(model);
            }

            
        }
        private void AddErrors(params string[] errors)
        {
            foreach (var error in errors)
            {
                ModelState.AddModelError("", error);
            }
        }


    }
}
