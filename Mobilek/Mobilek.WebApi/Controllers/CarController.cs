﻿using Mobilek.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Mobilek.WebApi.Controllers
{
    [Authorize]
    public class CarController : ApiController
    {
        readonly ICarRepository _carRepository;
        public CarController(ICarRepository repo)
        {
            _carRepository = repo;
        }
        // GET: api/Car
        public IHttpActionResult Get()
        {
            return this.Ok(_carRepository.GetAll());
        }

        // GET: api/Car/5
        public IHttpActionResult Get(int id)
        {
            //CarDTO car = _carRepository.GetAll().Single(s => s.Id == id);
            CarDTO car = _carRepository.Find(id);
            if (car == null)
            {
                return NotFound();
            }
            return this.Ok(car);
        }
        //Get: api/Car/Brand/BMW
        [ActionName("Brand")]
        public IHttpActionResult GetBrand(string name)
        {
            IEnumerable<CarDTO> cars = _carRepository.FindByBrand(name);
            if (cars == null || cars.Count() == 0)
            {
                return NotFound();
            }
            return this.Ok(cars);
        }
        [ActionName("Model")]
        public IHttpActionResult GetModel(string name)
        {
            IEnumerable<CarDTO> cars = _carRepository.FindByModel(name);
            if (cars == null || cars.Count() == 0)
            {
                return NotFound();
            }
            return this.Ok(cars);
        }
        [ActionName("Colour")]
        public IHttpActionResult GetColour(string name)
        {
            IEnumerable<CarDTO> cars = _carRepository.FindByColour(name);
            if (cars == null || cars.Count() == 0)
            {
                return NotFound();
            }
            return this.Ok(cars);
        }
        
        // POST: api/Car
        public IHttpActionResult Post([FromBody]CarDTO value)
        {
            _carRepository.Add(value);
            return this.Ok((true));

        }

        // PUT: api/Car/5
        public IHttpActionResult Put([FromBody]CarDTO value)
        {
            return this.Ok((_carRepository.Update(value)));
        }
    }
}
