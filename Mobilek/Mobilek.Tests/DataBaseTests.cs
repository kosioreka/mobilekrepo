﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mobilek.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobilek.Tests
{
    [TestClass]
    public class DataBaseTests
    {

        [TestMethod]
        public void StationInsertRemoveTest()
        {

            MobilekEntities entities = new MobilekEntities();

            string city = "test";

            var station = new Station { city = city, street = "test", streetNumer = 1, zipCode = "zipcode" };
            entities.Stations.Add(station);
            entities.SaveChanges();

            var dbStation = entities.Stations.FirstOrDefault(c => c.city == station.city && c.street == station.street);
            Assert.IsNotNull(dbStation);

            Assert.IsTrue(dbStation.streetNumer == station.streetNumer);
            Assert.IsTrue(dbStation.zipCode == station.zipCode);

            dbStation.city = "changed";
            entities.SaveChanges();
            Assert.IsFalse(dbStation.city == city);

            entities.Stations.Remove(dbStation);
            entities.SaveChanges();
            dbStation = entities.Stations.FirstOrDefault(c => c.city == station.city && c.street == station.street);

            Assert.IsNull(dbStation);

            entities.Dispose();
        }

        //[TestMethod]
        //public void StationEditTest()
        //{
        //    var entities = new MobilekEntities();

        //    Station st = new Station { street = "street", city = "city", zipCode = "zip", streetNumer = 5 };

        //    entities.Stations.Add(st);
        //    entities.SaveChanges();
        //    StationEditViewModel vm = new StationEditViewModel(st);
        //    vm.EntitiesEddit(st.Id, "good", "good", "good", "50");

        //    entities.Dispose();
        //    entities = new MobilekEntities();

        //    var dbSt = entities.Stations.Single(s => s.Id == st.Id);


        //    Assert.IsTrue(dbSt.street == "good" && dbSt.street == "good" && dbSt.zipCode == "good" && dbSt.streetNumer == 50);

        //    vm.EntitiesEddit(st.Id, "bad", "bad", "bad", "bad");
        //    entities.Dispose();
        //    entities = new MobilekEntities();
        //    dbSt = entities.Stations.Single(s => s.Id == st.Id);
        //    Assert.IsFalse(dbSt.street == "bad" && dbSt.street == "bad" && dbSt.zipCode == "bad");

        //    entities.Stations.Remove(dbSt);
        //    entities.SaveChanges();

        //}

        //[TestMethod]
        //public void CarAddViewModelConstrTest()
        //{
        //    var vm = new CarAddViewModel();

        //    using (var entities = new MobilekEntities())
        //    {
        //        var stations = entities.Stations;

        //        Assert.IsTrue(stations.Count() == vm.StationsCollection.Count);
        //    }
        //}
        //[TestMethod]
        //public void CarEditViewModelConstrTest()
        //{
        //    using (var entities = new MobilekEntities())
        //    {
        //        var car = entities.Cars.FirstOrDefault();

        //        if(car!= null)
        //        {
        //            CarEditViewModel vm = new CarEditViewModel(car);

        //            Assert.IsTrue(vm.Brand == car.brand);
        //            Assert.IsTrue(vm.Brand == car.brand);
        //            Assert.IsTrue(vm.Model == car.model);
        //            Assert.IsTrue(vm.Colour == car.colour);
        //            Assert.IsTrue(vm.ProductionDate == car.productionDate);
        //            Assert.IsTrue(vm.Price == car.price);
        //            Assert.IsTrue(car.isEfficient == vm.IsEfficient);

        //            var stations = entities.Stations;

        //            Assert.IsTrue(stations.Count() == vm.StationsCollection.Count);
        //            var st = stations.Single(s => s.Id == car.stationId);
        //            Assert.IsTrue(st.Id == vm.Station.Id);
        //        }
        //    }
        //}

        //[TestMethod]
        //public void CustomerInsertTest()
        //{
        //    var entities = new MobilekEntities();
        //    Customer customer = new Customer() { firstName = "testName", lastName = "testLastName", 
        //        phoneNumber = "phoneNr", userName="username", email="email", pesel="pesel", creditCardNumber="credit",
        //        password="password"
        //    };
        //    entities.Customers.Add(customer);
        //    entities.SaveChanges();
        //    var cSet = entities.Customers.Where(c => c.lastName == customer.lastName);

        //    Assert.IsNotNull(cSet);
        //    entities.Dispose();
        //}
    }

}
