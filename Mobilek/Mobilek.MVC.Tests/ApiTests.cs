﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mobilek.DTO;
using Mobilek.MVC.Controllers;
using Mobilek.MVC.Helpers;
using Mobilek.MVC.ViewModels;

namespace Mobilek.MVC.Tests
{

    [TestClass]
    public class ApiTests
    {
        [TestInitialize]
        public void Init()
        {
            Mapper.CreateMap<RegisterViewModel, CustomerDTO>()
                .ForMember(dest => dest.password, opt => opt.MapFrom(src => SecurePasswordHasher.Hash(src.password)));
            Mapper.CreateMap<CustomerDTO, RegisterViewModel>();
            Mapper.CreateMap<CarRecord, CarFromApi>()
               .ForMember(dest => dest.annee, opt => opt.MapFrom(src => src.fields.annee))
               .ForMember(dest => dest.carrosserie, opt => opt.MapFrom(src => src.fields.carrosserie))
               .ForMember(dest => dest.co2_g_km, opt => opt.MapFrom(src => src.fields.co2_g_km))
               .ForMember(dest => dest.consommation_extra_urbaine_l_100km, opt => opt.MapFrom(src => src.fields.consommation_extra_urbaine_l_100km))
               .ForMember(dest => dest.consommation_mixte_l_100km, opt => opt.MapFrom(src => src.fields.consommation_mixte_l_100km))
               .ForMember(dest => dest.co_type_i_g_km, opt => opt.MapFrom(src => src.fields.co_type_i_g_km))
               .ForMember(dest => dest.hc_g_km, opt => opt.MapFrom(src => src.fields.hc_g_km))
               .ForMember(dest => dest.hybride, opt => opt.MapFrom(src => src.fields.hybride))
               .ForMember(dest => dest.marque, opt => opt.MapFrom(src => src.fields.marque))
               .ForMember(dest => dest.masse_vide_euro_min_kg, opt => opt.MapFrom(src => src.fields.masse_vide_euro_min_kg))
               .ForMember(dest => dest.modele_dossier, opt => opt.MapFrom(src => src.fields.modele_dossier))
               .ForMember(dest => dest.puissance_maximale, opt => opt.MapFrom(src => src.fields.puissance_maximale))
               ;
            Mapper.CreateMap<CarDetails, CarFromApi>()
                .ForMember(dest => dest.annee, opt => opt.MapFrom(src => src.annee))
                .ForMember(dest => dest.carrosserie, opt => opt.MapFrom(src => src.carrosserie))
                .ForMember(dest => dest.co2_g_km, opt => opt.MapFrom(src => src.co2_g_km))
                .ForMember(dest => dest.consommation_extra_urbaine_l_100km, opt => opt.MapFrom(src => src.consommation_extra_urbaine_l_100km))
                .ForMember(dest => dest.consommation_mixte_l_100km, opt => opt.MapFrom(src => src.consommation_mixte_l_100km))
                .ForMember(dest => dest.co_type_i_g_km, opt => opt.MapFrom(src => src.co_type_i_g_km))
                .ForMember(dest => dest.hc_g_km, opt => opt.MapFrom(src => src.hc_g_km))
                .ForMember(dest => dest.hybride, opt => opt.MapFrom(src => src.hybride))
                .ForMember(dest => dest.marque, opt => opt.MapFrom(src => src.marque))
                .ForMember(dest => dest.masse_vide_euro_min_kg, opt => opt.MapFrom(src => src.masse_vide_euro_min_kg))
                .ForMember(dest => dest.modele_dossier, opt => opt.MapFrom(src => src.modele_dossier))
                .ForMember(dest => dest.puissance_maximale, opt => opt.MapFrom(src => src.puissance_maximale))
                ;
        }


        [TestMethod]
        public void GetUniqueBrandCarsTest()
        {
            var result = FrenchApi.GetUniqueBrandCars();
            Assert.IsNotNull(result);

            Assert.IsInstanceOfType(result,typeof(List<string>));
            Assert.IsTrue(result.Count!=0);
        }

        [TestMethod]
        [ExpectedException(typeof(WebException))]
        public void GetCarByIdNegativeTest()
        {
            var result = FrenchApi.GetCarById("test");
        }

        [TestMethod]
        public void GetUniqueBrandCarsAsyncTest()
        {
            var result = FrenchApi.GetUniqueBrandCarsAsync();

            var res = result.Result;
            Assert.IsNotNull(res);
            Assert.IsInstanceOfType(res, typeof(List<string>));
            Assert.IsTrue(res.Count != 0);
        }

        [TestMethod]
        public void GetCarByBrandAndModelNegativeTest()
        {
            var result = FrenchApi.GetCarByBrandAndModel("test", "test");

            Assert.IsNull(result);
        }

        [TestMethod]
        public void GetCarByBrandAndModelPositiveTest()
        {
            var brands = FrenchApi.GetUniqueBrandCars();

            if (brands.Count != 0)
            {
                var models = FrenchApi.GetCarsByBrand(brands[0]);

                if (models.Count != 0)
                {
                    var car = FrenchApi.GetCarByBrandAndModel(brands[0], models[0].modele_dossier);

                    Assert.IsNotNull(car);
                    Assert.IsInstanceOfType(car,typeof(CarFromApi));
                }

            }
            

        }



        [TestMethod]
        public void GetCarsByBrandPositiveTest()
        {
            var brands = FrenchApi.GetUniqueBrandCars();
            if (brands.Count != 0)
            {
                var result = FrenchApi.GetCarsByBrand(brands[0]);

                Assert.IsTrue(result.Count!=0);
                Assert.IsInstanceOfType(result,typeof(List<CarFromApi>));
            }
        }

        [TestMethod]
        public void GetCarsByBrandNegativeTest()
        {
            var brand = "ERRORBRAND";
            var result = FrenchApi.GetCarsByBrand(brand);

            Assert.IsTrue(result.Count==0);
            Assert.IsInstanceOfType(result,typeof(List<CarFromApi>));
            
        }




    }
}
