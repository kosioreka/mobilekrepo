﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobilek.MVC.Constants
{
    public class Roles
    {
        public const string AdminRole = "Admin";
        public const string UserRole = "User";
    }
}