﻿using GalaSoft.MvvmLight.Messaging;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Mobilek.DTO;
using Mobilek.Helper_Classes;
using Mobilek.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Mobilek
{
    /// <summary>
    /// The <c>CarEditViewModel</c> defines view for worker's editing cars properties functinonaltiy
    /// </summary>
    /// <remarks>
    /// The <c>CarEditViewModel</c> requires a car object, that will be edited
    /// </remarks>
    public class CarEditViewModel : ObservableObject, IPageViewModel
    {
        #region private fields
        private string brand;
        private string model;
        private string colour;
        private DateTime productionDate;
        private decimal price;
        private bool isEfficient;
        private int Id;
        private StationDTO station;
        private WorkerDTO w;
        ObservableCollection<StationDTO> _stationsCollection = new ObservableCollection<StationDTO>();
        private ICommand acceptCommand;
        private ICommand cancelCommand;
        #endregion

        #region properties
        public string Brand
        {
            get { return brand; }
            set
            {
                brand = value;
                OnPropertyChanged("Brand");
            }
        }
        public string Model
        {
            get { return model; }
            set
            {
                model = value;
                OnPropertyChanged("Model");
            }
        }
        public string Colour
        {
            get { return colour; }
            set
            {
                colour = value;
                OnPropertyChanged("Colour");
            }
        }
        public DateTime ProductionDate
        {
            get { return productionDate; }
            set
            {
                productionDate = value;
                OnPropertyChanged("ProductionDate");
            }
        }
        public decimal Price
        {
            get { return price; }
            set
            {
                price = value;
                OnPropertyChanged("Price");
            }
        }
        public bool IsEfficient
        {
            get { return isEfficient; }
            set
            {
                isEfficient = value;
                OnPropertyChanged("IsEfficient");
            }
        }
        public StationDTO Station
        {
            get { return station; }
            set
            {
                station = value;
                OnPropertyChanged("Station");
            }
        }
        public ObservableCollection<StationDTO> StationsCollection
        {
            get
            {
                return _stationsCollection;
            }
            set
            {
                _stationsCollection = value;
                OnPropertyChanged("StationsCollection");
            }
        }
        public string Name
        {
            get
            {
                return "Car Edit";
            }
        }
        #endregion

        #region commands
        public ICommand AcceptCommand
        {
            get
            {
                if (acceptCommand == null)
                {
                    acceptCommand = new RelayCommand(
                        param => Accept());

                }
                return acceptCommand;
            }
        }
        public ICommand CancelCommand
        {
            get
            {
                if (cancelCommand == null)
                {
                    cancelCommand = new RelayCommand(
                        param => Exit());

                }
                return cancelCommand;
            }
        }
        #endregion

        #region methods, constructor
        private async void Accept()
        {
            var service = new Mobilek.Services.CarDTOService(w.login,w.password);
            var c = await service.Get(Id);

            c.stationId = Station.Id;
            c.brand = Brand;
            c.colour = Colour;
            c.isEfficient = IsEfficient;
            c.model = Model;
            c.price = Price;
            c.productionDate = ProductionDate;

            await service.Put(c);

            var msg = new FireRefresh();
            Messenger.Default.Send<FireRefresh>(msg);
            Exit();
        }
        private async void Exit()
        {
            var metroWindow = App.Current.MainWindow as MetroWindow;
            var actualDialog = await metroWindow.GetCurrentDialogAsync<CustomDialog>();
            await metroWindow.HideMetroDialogAsync(actualDialog);
        }
        #region constructor
        public CarEditViewModel(CarDTO c,WorkerDTO w)
        {
            this.w = w;
            GetStations(c);
            Brand = c.brand;
            Model = c.model;
            Colour = c.colour;
            ProductionDate = c.productionDate;
            Price = c.price;
            IsEfficient = c.isEfficient;
            Id = c.Id;
        }
        private async void GetStations(CarDTO c)
        {
            var service = new StationDTOService(w.login,w.password);
            var stationss = await service.Get();
            var stations = stationss.AsEnumerable();

            StationsCollection = ToObservableCollectioncs.ToObservableCollection<StationDTO>(stations);

            var single = await service.Get(c.stationId);
            Station = single;
        }
        #endregion
        #endregion

    }
}
