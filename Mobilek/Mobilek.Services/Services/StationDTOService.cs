﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mobilek.DTO;
using System.Net.Http.Headers;

namespace Mobilek.Services
{
    public interface IStationDTOService
    {
        Task<StationDTO> Get(int id);

        Task<IList<StationDTO>> Get();

        Task<bool> Post(StationDTO item);

        Task<bool> Put(StationDTO item);

        Task<bool> Delete(int id);

    }


    public class StationDTOService : WebApiService,IStationDTOService
    {
        public StationDTOService(string l,string p) : base("Station")
        {
            this._httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue(
                    "Basic",
                    Convert.ToBase64String(
                        System.Text.ASCIIEncoding.ASCII.GetBytes(
                            string.Format("{0}:{1}", l, p))));
        }

        public async Task<StationDTO> Get(int id)
        {
            return await this.Get<StationDTO>(id.ToString());
        }
        public async Task<IList<StationDTO>> Get()
        {
            return await this.Get<IList<StationDTO>>("");
        }
        public async Task<bool> Post(StationDTO item)
        {
            return await this.Post<StationDTO, bool>("POST", item);       
        }
        public async Task<bool> Put(StationDTO item)
        {
            return await this.Put<StationDTO, bool > ("PUT", item);
        }
        public async Task<bool> Delete(int id)
        {
            return await this.Delete<StationDTO, bool>(id.ToString());
        }

        //public async Task<IList<StudentDetailsDto>> Search(string searchPhrase)
        //{
        //    return await this.Get<IList<StudentDetailsDto>>(string.Concat("Search/", HttpUtility.UrlEncode(searchPhrase)));
        //}
    }
}
