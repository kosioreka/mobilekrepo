﻿using AutoMapper;
using Mobilek.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper.QueryableExtensions;
using Mobilek.Domain;


namespace Mobilek.WebApi
{
    public interface ICarRepository
    {
        void Add(CarDTO item);
        IEnumerable<CarDTO> GetAll();
        CarDTO Find(int id);
        IEnumerable<CarDTO> FindByBrand(string brand);
        IEnumerable<CarDTO> FindByModel(string model);
        IEnumerable<CarDTO> FindByColour(string colour);
        //void Remove(int id);
        bool Update(CarDTO item);
    }
    public class CarRepository : ICarRepository,IDisposable
    {
        private MobilekEntities entities = new MobilekEntities();

        public void Add(CarDTO item)
        {
            entities.Cars.Add(Mapper.Map<Car>(item));
            entities.SaveChanges();
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (entities != null)
                    entities.Dispose();
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public CarDTO Find(int id)
        {
            //return Mapper.Map<CarDTO>(entities.Cars.Single(s => s.Id == id));
            //TODO
            var station = entities.Cars.FirstOrDefault(s => s.Id == id);
            if (station == null)
            {
                return null;
            }
           
            return Mapper.Map<CarDTO>(station);
            

        }
        public IEnumerable<CarDTO> FindByBrand(string brand)
        {
            return entities.Cars.OrderBy(c => c.model).Where(c => c.brand == brand).ProjectTo<CarDTO>();
        }
        public IEnumerable<CarDTO> FindByModel(string model)
        {
            return entities.Cars.OrderBy(c => c.brand).Where(c => c.model == model).ProjectTo<CarDTO>();
        }
        public IEnumerable<CarDTO> FindByColour(string colour)
        {
            return entities.Cars.OrderBy(c => c.brand).Where(c => c.colour == colour).ProjectTo<CarDTO>();
        }

        public IEnumerable<CarDTO> GetAll()
        {
            var k = entities.Cars.OrderBy(c=>c.brand).ProjectTo<CarDTO>();
            return k;
        }


        public bool Update(CarDTO item)
        {

            var car = entities.Cars.FirstOrDefault(s => s.Id == item.Id);
            if(car!=null)
            {
                car.brand = item.brand;
                car.colour = item.colour;
                car.isEfficient = item.isEfficient;
                car.model = item.model;
                car.price = item.price;
                car.productionDate = item.productionDate;
                car.stationId = item.stationId;
                entities.SaveChanges();
                return true;
            }
            return false;
        }
    }
}