﻿using Mobilek.DTO;
using Mobilek.MVC.Models;
using Mobilek.Services;
using Mobilek;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;
using Mobilek.MVC.Controllers;
using Mobilek.MVC.ViewModels;
using Mobilek.MVC.Constants;
using AutoMapper;
using Mobilek.MVC.Helpers;
using System.Text;
using System.Dynamic;
namespace MobilekMVC.Controllers
{
    [Authorize]
    public class UserAccountController : Controller
    {
        private readonly ICustomerDTOService _customerService;
        private readonly IOrderDTOService _orderService;
        private readonly IStationDTOService _stationService;
        private readonly ICarDTOService _carService;
        public UserAccountController(ICarDTOService carDtoService, IStationDTOService stationDtoService, IOrderDTOService orderDtoService, ICustomerDTOService customerDtoService)
        {
            _customerService = customerDtoService;
            _orderService = orderDtoService;
            _stationService = stationDtoService;
            _carService = carDtoService;
        }

        #region Profile

        [HttpGet]
        [Authorize(Roles = Roles.UserRole)]
        public ActionResult UserProfile()
        {
            var cust = Task.Run(() => _customerService.Get(User.Identity.Name)).Result;
            EditProfileViewModel regi = Mapper.Map<EditProfileViewModel>(cust);
            return View(regi);
        }

        [Authorize(Roles = Roles.UserRole)]
        [HttpPost]
        public ActionResult UserProfile(EditProfileViewModel model, string returnUrl)
        {
            string msg;
            CustomerDTO cust = new CustomerDTO();
            try
            {
                if (ModelState.IsValid)
                {
                    cust = Task.Run(() => _customerService.Get(User.Identity.Name)).Result;
                    model.password = cust.password;
                    CustomerDTO customer = Mapper.Map<CustomerDTO>(model);
                    customer.Id = cust.Id;
                    
                    var custPut = Task.Run((() => _customerService.Put(customer))).Result;
                    if (custPut)
                        msg = "Saved Successfully";
                    else
                    {
                        msg = "Something went wrong";
                        ModelState.AddModelError("", msg);
                        RedirectToRoute(returnUrl);
                    }
                }
                else
                {
                    msg = "Validation profie not successfull";
                    ModelState.AddModelError("", msg);
                    RedirectToRoute(returnUrl);
                }
            }
            catch (Exception ex)
            {
                msg = "Error occured:" + ex.Message;
                ModelState.AddModelError("", msg);
                RedirectToRoute(returnUrl);
            }

            model.IsEdited = true;

            //model = Mapper.Map<EditProfileViewModel>(cust);
            return View(model);
        }
        #endregion

        #region Order History
        [HttpGet]
        [Authorize(Roles = Roles.UserRole)]
        public ActionResult OrderHistory()
        {
            List<OrderModel> allOrders = new List<OrderModel>();
            var orders = GetOrders();
            foreach (OrderDTO o in orders)
            {
                var carList = GetCars(o.Id);
                allOrders.Add(new OrderModel { order = o, cars = carList });
            }
            return View(allOrders);
        }

        public List<OrderDTO> GetOrders()
        {
            var orders = Task.Run(() => _orderService.Get()).Result;
            var orderList = orders.Where(o => o.Customer.userName.Equals(User.Identity.Name));
            return orderList.ToList();
        }
        private List<CarDTO> GetCars(int id)
        {
            var order = Task.Run(() => _orderService.Get(id)).Result;
            return order.Cars.ToList();
        }
        #endregion

        #region Make Order

        [HttpGet]
        [Authorize(Roles = Roles.UserRole)]
        public ActionResult MakeOrder()
        {
            List<SelectListItem> cars = Task.Run(() => _carService.Get()).Result
                .Select(s => new SelectListItem()
                {
                    Value = s.Id.ToString(),
                    Text = s.brand + " " + s.model

                }).ToList();
            ViewBag.cars = cars;

            var stations = GetStations();
            IEnumerable<SelectListItem> items = Task.Run(() => _stationService.Get()).Result.Select(c => new SelectListItem
            {
                Value = c.Id.ToString(),
                Text = c.street + " " + c.streetNumer
            }).ToList();
            ViewBag.Stationss = items;
            return View();
        }

        [HttpPost]
        [Authorize(Roles = Roles.UserRole)]
        public ActionResult MakeOrder(MakeOrderModel model)
        {
            string msg = "";
            if (ModelState.IsValid)
            {
                try{
                    var cust = Task.Run(() => _customerService.Get(User.Identity.Name)).Result;
                    StationDTO station = Task.Run(() => _stationService.Get(model.stationItem)).Result;
                    List<CarDTO> cars = new List<CarDTO>();
                    foreach (int i in model.carItems)
                    {
                        cars.Add(Task.Run(() => _carService.Get(i)).Result);
                    }
                    OrderDTO order = new OrderDTO()
                    {
                        customerId = cust.Id,
                        endDate = model.endingDate,
                        startDate = model.startingDate,
                        returnStationId = station.Id,
                        isFinished = (model.endingDate<DateTime.Now),
                        workerId = 1,
                        Cars = cars
                    };
                    var result = Task.Run(() => _orderService.Post(order)).Result;
                }catch(Exception e){
                    msg = "Error occured:" + e.Message;
                }
            }
            else
            {
                msg = "Oder submission not successfull";
            }
            if (msg == "")
            {

            }
            else
            {
                ModelState.AddModelError("", msg);
            }

            List<SelectListItem> carList = Task.Run(() => _carService.Get()).Result
                .Select(s => new SelectListItem()
                {
                    Value = s.Id.ToString(),
                    Text = s.brand + " " + s.model

                }).ToList();
            ViewBag.cars = carList;

            IEnumerable<SelectListItem> items = Task.Run(() => _stationService.Get()).Result.Select(c => new SelectListItem
            {
                Value = c.Id.ToString(),
                Text = c.street + " " + c.streetNumer
            });
            ViewBag.Stationss = items.ToList();
            return View();
        }

       public List<StationDTO> GetStations()
        {
            var stations = Task.Run(() => _stationService.Get()).Result;
            return stations.ToList();
        }
       
        [HttpPost]
        [Authorize(Roles = Roles.UserRole)]
        public string GetModels(string brand)
        {
            var models = FrenchApi.GetCarsByBrand(brand).Select(s => s.modele_dossier).ToList();

            StringBuilder result = new StringBuilder();
            foreach (var s in models)
            {
                result.Append("<option value=" + '\"' + s + '\"' + " > " + s + " </option> ");
            }
            return result.ToString();

        }
        #endregion
    }
}
