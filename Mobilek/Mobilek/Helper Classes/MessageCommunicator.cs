﻿using Mobilek.DTO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobilek
{
    /// <summary>
    /// Class used for communication between views  
    /// </summary>
    public class MessageCommunicatorNewOrder
    {
        public Collection<CarDTO> carsCollection { get; set; }
        public StationDTO startingStation { get; set; }
        public StationDTO endingStation { get; set; }
        public bool submittedOrder { get; set; }
        public CustomerDTO customer { get; set; }
    }
    public class MessageCommunicatorProfileEdit
    {
        public bool isModified { get; set; }
        public CustomerDTO customer { get; set; }

    }
}
