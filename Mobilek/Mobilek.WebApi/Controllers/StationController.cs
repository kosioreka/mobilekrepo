﻿using Mobilek.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;

namespace Mobilek.WebApi.Controllers
{
    [Authorize]
    public class StationController : ApiController
    {
        readonly IStationRepository _stationRepository;
        public StationController(IStationRepository repo)
        {
            _stationRepository = repo;
        }
        // GET: api/Station

        public IHttpActionResult Get()
        {
            var st = _stationRepository.GetAll();
            return this.Ok(st);
        }

        // GET: api/Station/5
        public IHttpActionResult Get(int id)
        {
            StationDTO station = _stationRepository.Find(id);
            if (station == null)
            {
                return NotFound();
            }
            return this.Ok(station);
        }
        //GET: api/Station/City/Warszawa
        [ActionName("City")]
        public IHttpActionResult Get(string name)
        {
            IEnumerable<StationDTO> station = _stationRepository.FindByCity(name);
            if (station == null || station.Count() == 0)
            {
                return NotFound();
            }
            return this.Ok(station);
        }

        // POST: api/Station
        public IHttpActionResult Post([FromBody]StationDTO value)
        {
            _stationRepository.Add(value);
            return this.Ok(true);
        }

        // PUT: api/Station/5
        public IHttpActionResult Put([FromBody]StationDTO value)
        {
            _stationRepository.Update(value);
            return this.Ok(true);
        }

        // DELETE: api/Station/5
        public IHttpActionResult Delete(int id)
        {
            return this.Ok(_stationRepository.Remove(id));
        }
    }
}
