﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace Mobilek.Helper_Classes
{
    public class AutomapperConfig
    {
        public static void Configure()
        {
            Mapper.CreateMap<CarRecord, CarFromApi>()
                .ForMember(dest => dest.annee, opt => opt.MapFrom(src => src.fields.annee))
                .ForMember(dest => dest.carrosserie, opt => opt.MapFrom(src => src.fields.carrosserie))
                .ForMember(dest => dest.co2_g_km, opt => opt.MapFrom(src => src.fields.co2_g_km))
                .ForMember(dest => dest.consommation_extra_urbaine_l_100km, opt => opt.MapFrom(src => src.fields.consommation_extra_urbaine_l_100km))
                .ForMember(dest => dest.consommation_mixte_l_100km, opt => opt.MapFrom(src => src.fields.consommation_mixte_l_100km))
                .ForMember(dest => dest.co_type_i_g_km, opt => opt.MapFrom(src => src.fields.co_type_i_g_km))
                .ForMember(dest => dest.hc_g_km, opt => opt.MapFrom(src => src.fields.hc_g_km))
                .ForMember(dest => dest.hybride, opt => opt.MapFrom(src => src.fields.hybride))
                .ForMember(dest => dest.marque, opt => opt.MapFrom(src => src.fields.marque))
                .ForMember(dest => dest.masse_vide_euro_min_kg, opt => opt.MapFrom(src => src.fields.masse_vide_euro_min_kg))
                .ForMember(dest => dest.modele_dossier, opt => opt.MapFrom(src => src.fields.modele_dossier))
                .ForMember(dest => dest.puissance_maximale, opt => opt.MapFrom(src => src.fields.puissance_maximale))
                ;
            Mapper.CreateMap<CarDetails, CarFromApi>()
                .ForMember(dest => dest.annee, opt => opt.MapFrom(src => src.annee))
                .ForMember(dest => dest.carrosserie, opt => opt.MapFrom(src => src.carrosserie))
                .ForMember(dest => dest.co2_g_km, opt => opt.MapFrom(src => src.co2_g_km))
                .ForMember(dest => dest.consommation_extra_urbaine_l_100km, opt => opt.MapFrom(src => src.consommation_extra_urbaine_l_100km))
                .ForMember(dest => dest.consommation_mixte_l_100km, opt => opt.MapFrom(src => src.consommation_mixte_l_100km))
                .ForMember(dest => dest.co_type_i_g_km, opt => opt.MapFrom(src => src.co_type_i_g_km))
                .ForMember(dest => dest.hc_g_km, opt => opt.MapFrom(src => src.hc_g_km))
                .ForMember(dest => dest.hybride, opt => opt.MapFrom(src => src.hybride))
                .ForMember(dest => dest.marque, opt => opt.MapFrom(src => src.marque))
                .ForMember(dest => dest.masse_vide_euro_min_kg, opt => opt.MapFrom(src => src.masse_vide_euro_min_kg))
                .ForMember(dest => dest.modele_dossier, opt => opt.MapFrom(src => src.modele_dossier))
                .ForMember(dest => dest.puissance_maximale, opt => opt.MapFrom(src => src.puissance_maximale))
                ;
        }
            
    }
}
