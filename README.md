# README #

Designed a program for easy car rental between designated stations. The project consists of:

* Desktop application for the user and the administratior
* RESTful API Service – service, that provides access to the database.
* Web application

Applied technologies and functionalities in the project:
C# 5.0, .NET 4.5 (WPF, Entity Framework, ASP.NET MVC), MVVM, WebApi, Linq, JSON.NET, Visual Studio 2015, MS SQL, JavsScript, jQuery, Moq