﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Mobilek.DTO;
using Mobilek.MVC.Constants;
using Mobilek.Services;

namespace Mobilek.MVC.Helpers
{
    public class AccountHelper
    { 
        public static ClaimsIdentity ValidateRegistry(CustomerDTO c,ICustomerDTOService _customerDtoService,out string error)
        {
            error = string.Empty;
            var alreadyAdded = Task.Run(() => _customerDtoService.Get(c.userName)).Result;
            if (alreadyAdded != null)
            {
                error = Messages.UserExists;
                return null;
            }
            var isOk = Task.Run((() => _customerDtoService.Post(c))).Result;
            if (isOk)
            {
                var user = Task.Run((() => _customerDtoService.Get((c.userName)))).Result;
                IList<Claim> claimCollection = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.userName),
                    new Claim(ClaimTypes.Role,Roles.UserRole)
                };
                var claimsIdentity = new ClaimsIdentity(claimCollection, DefaultAuthenticationTypes.ApplicationCookie);
                return claimsIdentity;
            }
            else
            {
                error = Messages.UnexpecedDBError;
                return null;
            }
        }

        public static ClaimsIdentity ValidateLogin(string Login,string Password, ICustomerDTOService _customerDtoService,IWorkerDTOService _workerDtoService, out string error)
        {
            error = string.Empty;


            var customer = Task.Run(() => _customerDtoService.Get((Login))).Result;

            var admin = Task.Run(() => _workerDtoService.Get((Login))).Result;

            if (customer != null)
            {
                if (SecurePasswordHasher.Verify(Password, customer.password))
                {
                    IList<Claim> claimCollection = new List<Claim>
                            {
                                new Claim(ClaimTypes.Name, customer.userName),
                                new Claim(ClaimTypes.Role,Roles.UserRole)
                            };

                    var claimsIdentity = new ClaimsIdentity(claimCollection,
                        DefaultAuthenticationTypes.ApplicationCookie);


                    return claimsIdentity;
                    
                }
                else
                {
                    error = Messages.IncorrectPassword;
                }

            }
            else if (admin != null)
            {
                if (SecurePasswordHasher.Verify(Password, admin.password))
                {
                    IList<Claim> claimCollection = new List<Claim>
                            {
                                new Claim(ClaimTypes.Name, admin.login),
                                new Claim(ClaimTypes.Role,Roles.AdminRole)
                            };

                    var claimsIdentity = new ClaimsIdentity(claimCollection,
                        DefaultAuthenticationTypes.ApplicationCookie);


                    return claimsIdentity;
                }
                else
                {
                    error = Messages.IncorrectPassword;
                }
            }
            else
            {
                error = Messages.NoUser;
            }

            return null;
        }

        public static bool ValidateChangePassword(
            Claim role,
            string Login,
            string OldPassword,
            string NewPassword,
            ICustomerDTOService _customerDtoService,
            IWorkerDTOService _workerDtoService,
            out string error)
        {
            error = string.Empty;
            if (role != null)
            {
                if (role.Value == Roles.AdminRole)
                {

                    var worker = Task.Run((() => _workerDtoService.Get(Login))).Result;
                    if (worker != null)
                    {
                        if (SecurePasswordHasher.Verify(OldPassword, worker.password))
                        {
                            worker.password = SecurePasswordHasher.Hash(NewPassword);
                            if (Task.Run(() => _workerDtoService.Put(worker)).Result)
                            {
                                return true;
                            }
                        }
                        else
                        {
                            error = Messages.IncorrectPassword;
                            return false;
                        }
                    }
                }
                else
                {
                    var customer = Task.Run((() => _customerDtoService.Get((Login)))).Result;
                    if (customer != null)
                    {
                        if (SecurePasswordHasher.Verify(OldPassword, customer.password))
                        {
                            customer.password = SecurePasswordHasher.Hash(NewPassword);
                            if (Task.Run(() => _customerDtoService.Put(customer)).Result)
                            {
                                return true;
                            }
                        }
                        else
                        {

                            error = Messages.IncorrectPassword;
                            return false;
                        }
                    }
                }
            }
            error = Messages.UnexcpectedError;
            return false;
        }
    }
}