﻿using Mobilek.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Mobilek.Services
{
    public interface IWorkerDTOService
    {
        Task<WorkerDTO> Get(string id);

        Task<IList<WorkerDTO>> Get();

        Task<bool> Put(WorkerDTO item);

    }
    public class WorkerDTOService : WebApiService,IWorkerDTOService
    {
        
        public WorkerDTOService(string l, string p) : base("Worker")
        {


            this._httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue(
                    "Basic",
                    Convert.ToBase64String(
                        System.Text.ASCIIEncoding.ASCII.GetBytes(
                            string.Format("{0}:{1}", l, p))));


        }

        public async Task<WorkerDTO> Get(string id)
        {
            return await this.Get<WorkerDTO>(id);
        }
        public async Task<IList<WorkerDTO>> Get()
        {
            return await this.Get<IList<WorkerDTO>>("");
        }
        public async Task<bool> Put(WorkerDTO item)
        {
            return await this.Put<WorkerDTO, bool>("PUT", item);
        }
    }
}
