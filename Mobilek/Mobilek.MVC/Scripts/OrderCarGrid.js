﻿$(function () {

    $("#orderCarGrid").jqGrid({
    url: "/UserAccount/GetCars",
    datatype: 'json',
    mtype: 'Get',
    colNames: ['Id', 'Brand', 'Model', 'Price', 'Colour', 'Station', 'Production date'],
    colModel: [
        { key: true, hidden: true, name: 'Id', index: 'Id', editable: true },
        { key: false, name: 'brand', index: 'brand', editable: false },
        { key: false, name: 'model', index: 'model', editable: false },
        { key: false, name: 'price', index: 'price', editable: false },
        { key: false, name: 'colour', index: 'colour', editable: false },
        { key: false, name: 'stationName', index: 'stationName', editable: false},
        { key: false, name: 'productionDate', index: 'productionDate', editable: false, formatter: 'date', formatoptions: { newformat: 'd/m/Y' } }],
    pager: jQuery('#pager'),
    rowNum: 10,
    rowList: [10, 20, 30, 40],
    height: '100%',
    viewrecords: true,
    caption: 'Cars',
    emptyrecords: 'No records to display',
    jsonReader: {
        root: "rows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems: false,
        Id: "0"
    },
    autowidth: true,
    multiselect: false
    }).navGrid('#pager', { edit: false, add: false, del: false, search: true, refresh: true })
});