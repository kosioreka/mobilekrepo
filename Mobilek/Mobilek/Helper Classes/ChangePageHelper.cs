﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobilek
{
    /// <summary>
    /// Class for changing the current view
    /// </summary>
    class ChangePageHelper
    {
        public IPageViewModel PageName { get; set; }
    }
}
