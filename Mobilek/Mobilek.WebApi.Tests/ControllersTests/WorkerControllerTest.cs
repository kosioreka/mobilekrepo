﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Mobilek.DTO;
using Mobilek.WebApi.Controllers;
using System.Web.Http;
using System.Web.Http.Results;
using Mobilek.Domain;
using AutoMapper;

namespace Mobilek.WebApi.Tests
{
    [TestClass]
    public class WorkerControllerTest
    {
        [TestInitialize]
        public void testInit()
        {
            Mobilek.WebApi.AutomapperConfigure.Configure();
        }
        [TestMethod]
        public void GetAllWorkers_Test()
        {

            var mockRepo = new Mock<IWorkerRepository>();
            mockRepo.Setup(s => s.GetAll()).Returns(new List<WorkerDTO>() { new WorkerDTO() });

            var controller = new WorkerController(mockRepo.Object);

            var getallResult = controller.Get();

            Assert.IsInstanceOfType(getallResult, typeof(OkNegotiatedContentResult<IEnumerable<WorkerDTO>>));

            var workers = getallResult as OkNegotiatedContentResult<IEnumerable<WorkerDTO>>;
            Assert.IsTrue(workers.Content.Count() == 1);

        }
        [TestMethod]
        public void FindWorker_Test()
        {
            
            var mockRepo = new Mock<IWorkerRepository>();
            int id = 5;
            int noid = 33;

            mockRepo.Setup(s => s.Find("5")).Returns(new WorkerDTO() {id=id } );

            var controller = new WorkerController(mockRepo.Object);

            var getallResult = controller.Get(id.ToString());

            Assert.IsInstanceOfType(getallResult, typeof(OkNegotiatedContentResult<WorkerDTO>));

            var worker = getallResult as OkNegotiatedContentResult<WorkerDTO>;

            Assert.IsTrue(worker.Content.id==id);

            

            var getresults2 = controller.Get(noid.ToString());
            Assert.IsInstanceOfType(getresults2, typeof(NotFoundResult));





        }


    }
}
