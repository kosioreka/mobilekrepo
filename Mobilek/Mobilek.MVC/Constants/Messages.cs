﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobilek.MVC.Constants
{
    public class Messages
    {
        public const string UserExists = "User already exists.";
        public const string IncorrectPassword = "Incorrect password.";
        public const string NoUser = "User does not exist.";
        public const string UnexpecedDBError = "Unexpected database error";
        public const string UnexcpectedError = "Unexpected error";

        public const string SuccessAdd = "Saved Successfully";
        public const string ValidationFail = "Validation data not sucessfull";
        public const string ErrorOccured = "Error occured:";
        public const string SuccessDelete = "Deleted Successfully";
        public const string StationDeleteFail = "Station already has registered cars - cannot be deleted";

    }
}