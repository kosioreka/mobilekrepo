﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Mobilek.DTO;

namespace Mobilek.Converters
{
    class BoolToStatusConverter :IValueConverter 
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if ((DateTime)value < DateTime.Today)
            {
                return "finished";
            }
            return "ongoing";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    //class StationConverter : IValueConverter
    //{

    //    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        if (value != null)
    //        {
    //            using (var entities = new MobilekEntities())
    //            {
    //                var id = int.Parse(value.ToString());
    //                var station = entities.Stations.FirstOrDefault<Station>(x => x.Id == id);
    //                //var service = new Services.StationDTOService();
    //                //var statio = service.Get().Result;
    //                //var station = statio.Single(s => s.Id == id);
    //                //var station = service.Get(id).Result;
    //                //GetStation(id);

    //                return station.street;
    //            }
    //        }
    //        return value;

    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    class CarConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                //using (var entities = new MobilekEntities())
                {
                    ////var id = int.Parse(value.ToString());
                    ////if (id != 0)
                    ////{
                    ////    var service = new Mobilek.Services.CarDTOService();
                    ////    var car = service.Get(id).Result;
                    ////    //var car = entities.Cars.FirstOrDefault<Car>(x => x.Id == id);
                    ////    if (car != null)
                    ////        return car.brand + " " + car.model;
                    ////}
                    ////else
                    ////{
                        return "<click to view>";
                    //}
                }
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    class ValueEmptyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if ((String)value == "" || (String)value == null)
                {
                    return "<not specified>";
                }
            }
            catch
            {
                return value;
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
