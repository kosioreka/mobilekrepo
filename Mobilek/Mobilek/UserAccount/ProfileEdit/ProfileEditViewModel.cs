﻿using GalaSoft.MvvmLight.Messaging;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Mobilek.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Mobilek
{
    /// <summary>
    /// The <c>ProfileEditViewModel</c> defines view (dialog) for editing profiles detailes (user's personal data)
    /// </summary>
    /// <remarks>
    /// The changes can be submitted or canceled
    /// </remarks>
    class ProfileEditViewModel :ObservableObject, IPageViewModel
    {
        #region private fields
        private CustomerDTO theCustomer;
        private ICommand acceptCommand;
        private ICommand cancelCommand;
        private string _firstName;
        private string _lastName;
        private string _pesel;
        private string _email;
        private string _phoneNr;
        private string _creditCardNr;
        #endregion

        #region properties
        public CustomerDTO TheCustomer { get { return theCustomer; } set { theCustomer = value; } }
        public string FirstName { 
            get { return _firstName; } 
            set { _firstName = value; 
                OnPropertyChanged("FirstName");
            }
        }
        public string LastName
        {
            get { return _lastName; }
            set
            {
                _lastName = value;
                OnPropertyChanged("LastName");
            }
        }
        public string Pesel
        {
            get { return _pesel; }
            set
            {
                _pesel = value;
                OnPropertyChanged("Pesel");
            }
        }
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                OnPropertyChanged("Email");
            }
        }
        public string PhoneNr
        {
            get { return _phoneNr; }
            set
            {
                _phoneNr = value;
                OnPropertyChanged("PhoneNr");
            }
        }
        public string CreditCardNr
        {
            get { return _creditCardNr; }
            set
            {
                _creditCardNr = value;
                OnPropertyChanged("CreditCardNr");
            }
        }
        public string Name
        {
            get { return "Profile Edit"; }
        }
        #endregion 
        
        #region commands
        public ICommand AcceptCommand
        {
            get
            {
                if (acceptCommand == null)
                {
                    acceptCommand = new RelayCommand(
                        param => Accept());

                }
                return acceptCommand;
            }
        }
        public ICommand CancelCommand
        {
            get
            {
                if (cancelCommand == null)
                {
                    cancelCommand = new RelayCommand(
                        param => Exit());

                }
                return cancelCommand;
            }
        }
        #endregion

        #region methods
        public ProfileEditViewModel(CustomerDTO customer)
        {
            TheCustomer = customer;
            FirstName = customer.firstName;
            LastName = customer.lastName;
            Email = customer.email;
            PhoneNr = customer.phoneNumber;
            CreditCardNr = customer.creditCardNumber;
            Pesel = customer.pesel;
        }
        private async void Accept()
        {
            //using (var entities = new MobilekEntities())
            {
                var service = new Mobilek.Services.CustomerDTOService(TheCustomer.userName,TheCustomer.password);
                CustomerDTO c = new CustomerDTO();
                c.firstName = FirstName;
                c.lastName = LastName;
                c.phoneNumber = PhoneNr;
                c.pesel = Pesel;
                c.email = Email;
                c.creditCardNumber = CreditCardNr;
                c.Id = theCustomer.Id;
                service.Put(c);
                TheCustomer = c;
            }
            var msg = new FireRefresh();
            Messenger.Default.Send<FireRefresh>(msg);
            SendIsModifiedCollectionInfo();
            Exit();
        }
        private async void Exit()
        {
            var metroWindow = App.Current.MainWindow as MetroWindow;
            var actualDialog = await metroWindow.GetCurrentDialogAsync<CustomDialog>();
            await metroWindow.HideMetroDialogAsync(actualDialog);
        }
        private void SendIsModifiedCollectionInfo()
        {
            if (true)//what condition?
            {
                Messenger.Default.Send<MessageCommunicatorProfileEdit>(new MessageCommunicatorProfileEdit()
                {
                    isModified = true,
                    customer = theCustomer
                });
            }
        }
        #endregion
    }
}
