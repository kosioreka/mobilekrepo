﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mobilek.MVC.Controllers;
using Mobilek.Services;
using Moq;

namespace Mobilek.MVC.Tests
{
    [TestClass]
    public class ManageControllerTests
    {
        [TestMethod]
        public void IndexReturnTypeTest()
        {
            var mockService = new Mock<ICustomerDTOService>();
            var mockService2 = new Mock<IWorkerDTOService>();
            var controller = new ManageController(mockService.Object, mockService2.Object);
            var result = controller.Index();

            Assert.IsInstanceOfType(result, typeof(ViewResult));

        }
        [TestMethod]
        public void ChangePasswordReturnTypeTest()
        {
            var mockService = new Mock<ICustomerDTOService>();
            var mockService2 = new Mock<IWorkerDTOService>();

            var controller = new ManageController(mockService.Object, mockService2.Object);
            var result = controller.ChangePassword();

            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }


    }
}
