﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Mobilek.DTO;
using Mobilek.MVC.Constants;
using Mobilek.MVC.Helpers;
using Mobilek.Services;

namespace Mobilek.MVC.Controllers
{

    public class WorkerController : Controller
    {

        private readonly ICarDTOService _carDtoService;
        private readonly IStationDTOService _stationjDtoService;
        public WorkerController(ICarDTOService carDtoService,IStationDTOService stationDtoService)
        {
            _carDtoService = carDtoService;
            _stationjDtoService = stationDtoService;
        }

        [Authorize(Roles = Roles.AdminRole)]
        // GET: Worker
        public ActionResult Index()
        {
            return View();
        }

        #region car
        [HttpGet]
        public JsonResult GetCars(string sidx, string sord, int page, int rows)  //Gets the todo Lists.
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            var records = Task.Run(() => _carDtoService.Get()).Result.AsQueryable();

            int totalRecords = records.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord.ToUpper() == "DESC")
            {
                records = records.OrderByDescending(s => s.brand);
                records = records.Skip(pageIndex * pageSize).Take(pageSize);
            }
            else
            {
                records = records.OrderBy(s => s.brand);
                records = records.Skip(pageIndex * pageSize).Take(pageSize);
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = records
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetStationCars()
        {
            var stations = Task.Run((() => _stationjDtoService.Get())).Result.Select(s=>s.city + " "+s.street);
            return Json(stations, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = Roles.AdminRole)]
        [HttpPost]
        public string CreateCar([Bind(Exclude = "Id")] CarDTO objTodo)
        {
            string msg;
            try
            {
                if (ModelState.IsValid)
                {
                    
                    var mod= FrenchApi.GetCarByBrandAndModel(objTodo.brand,objTodo.model);
                    var stations = Task.Run(() => _stationjDtoService.Get()).Result;
                    var station = stations.FirstOrDefault(s => objTodo.stationName.Contains(s.street));

                    if (mod != null && station!=null)
                    {
                        objTodo.frenchId = mod.recordid;
                        objTodo.stationId = station.Id;
                        var carpost = Task.Run((() => _carDtoService.Post(objTodo))).Result;
                        msg = Messages.SuccessAdd;
                    }
                    else
                    {
                        msg = Messages.ValidationFail;
                    }
                    
                }
                else
                {
                    msg = Messages.ValidationFail;
                }
            }
            catch (Exception ex)
            {
                msg = Messages.ErrorOccured + ex.Message;
            }
            return msg;
        }
        [HttpGet]
        public JsonResult GetFrenchCars()
        {
            var brandNames = FrenchApi.GetUniqueBrandCars(); 
            return Json(brandNames, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public string GetModels(string brand)
        {
            var models = FrenchApi.GetCarsByBrand(brand).Select(s => s.modele_dossier).ToList();

            StringBuilder result = new StringBuilder();
            foreach (var s in models)
            {
                result.Append("<option value=" + '\"' + s + '\"' + " > " + s + " </option> ");
            }
            return result.ToString();

        }
        [Authorize(Roles = Roles.AdminRole)]
        [HttpPost]
        public string EditCar(CarDTO objTodo)
        {
            string msg;
            try
            {
                if (ModelState.IsValid)
                {

                    var mod = FrenchApi.GetCarByBrandAndModel(objTodo.brand, objTodo.model);

                    var stations = Task.Run((() => _stationjDtoService.Get())).Result;
                    var station = stations.FirstOrDefault(s => objTodo.stationName.Contains(s.city));
                    if (station!=null && mod!=null)
                    {
                        objTodo.stationId = station.Id;
                        objTodo.frenchId = mod.recordid;
                    }

                    var carput = Task.Run((() => _carDtoService.Put(objTodo))).Result;
                    if(carput)
                        msg = Messages.SuccessAdd;
                    else
                    {
                        msg = Messages.UnexcpectedError;
                    }
                }
                else
                {
                    msg = Messages.ValidationFail;
                }
            }
            catch (Exception ex)
            {
                msg = Messages.ErrorOccured + ex.Message;
            }
            return msg;
        }


        #endregion


        #region Station
        [Authorize(Roles = Roles.AdminRole)]
        [HttpGet]
        public JsonResult GetStations(string sidx, string sord, int page, int rows)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            var records = Task.Run(() => _stationjDtoService.Get()).Result.AsQueryable();

            int totalRecords = records.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sord.ToUpper() == "DESC")
            {
                records = records.OrderByDescending(s => s.city);
                records = records.Skip(pageIndex * pageSize).Take(pageSize);
            }
            else
            {
                records = records.OrderBy(s => s.city);
                records = records.Skip(pageIndex * pageSize).Take(pageSize);
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = records
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        [Authorize(Roles = Roles.AdminRole)]
        public string CreateStation([Bind(Exclude = "Id")] StationDTO objTodo)
        {
            string msg;
            try
            {
                if (ModelState.IsValid)
                {
                    var carpost = Task.Run((() => _stationjDtoService.Post(objTodo))).Result;
                    msg = Messages.SuccessAdd;
                }
                else
                {
                    msg = Messages.ValidationFail;
                }
            }
            catch (Exception ex)
            {
                msg = Messages.ErrorOccured + ex.Message;
            }
            return msg;
        }
        [Authorize(Roles = Roles.AdminRole)]
        public string EditStation(StationDTO objTodo)
        {
            string msg;
            try
            {
                if (ModelState.IsValid)
                {
                    var carpost = Task.Run((() => _stationjDtoService.Put(objTodo))).Result;
                    msg = Messages.SuccessAdd;
                }
                else
                {
                    msg = Messages.ValidationFail;
                }
            }
            catch (Exception ex)
            {
                msg = Messages.ErrorOccured + ex.Message;
            }
            return msg;
        }
        [Authorize(Roles = Roles.AdminRole)]
        public string DeleteStation(int Id)
        {
            var station =Task.Run(() => _stationjDtoService.Get(Id)).Result;
            if (station != null)
            {
                var result = Task.Run(() => _stationjDtoService.Delete(Id)).Result;
                if (result)
                {
                    return Messages.SuccessDelete;
                }
            }
            return Messages.StationDeleteFail;
        }
        #endregion
    }
}