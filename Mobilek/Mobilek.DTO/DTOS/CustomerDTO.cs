﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobilek.DTO
{
    public class CustomerDTO
    {
        public int Id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string pesel { get; set; }
        public string phoneNumber { get; set; }
        public string creditCardNumber { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public string email { get; set; }

    }
}
