﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Mobilek.DTO;
using Mobilek.MVC.Constants;
using Mobilek.MVC.Helpers;
using Mobilek.MVC.ViewModels;
using Mobilek.Services;

namespace Mobilek.MVC.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly ICustomerDTOService _customerDtoService;
        private readonly IWorkerDTOService _workerDtoService;

        public AccountController(ICustomerDTOService customerDtoService, IWorkerDTOService workerDtoService)
        {
            _customerDtoService = customerDtoService;
            _workerDtoService = workerDtoService;
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string msg;
                    var claims = AccountHelper.ValidateLogin(model.Login, model.Password, _customerDtoService,
                        _workerDtoService, out msg);

                    if (claims != null)
                    {
                        AuthenticationManager.SignIn(claims);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        AddErrors(msg);
                        return View(model);
                    }
                }
                catch (Exception ex)
                {
                    return View(model);
                }
            }
            return View(model);
        }
        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                CustomerDTO c = Mapper.Map<CustomerDTO>(model);
                try
                {
                    string msg;
                    var claims = AccountHelper.ValidateRegistry(c, _customerDtoService, out msg);

                    if (claims == null)
                    {
                        AddErrors(msg);
                        return View(model);
                    }
                    else
                    {
                        AuthenticationManager.SignIn(claims);
                        return RedirectToAction("Index", "Home");
                    }
                }
                catch (Exception ex)
                {
                    AddErrors(Messages.UnexcpectedError);
                    return View(model);
                }
            }
            return View(model);
        }

        // POST: /Account/LogOff
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        { 
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
        private void AddErrors(params string[] errors)
        {
            foreach (var error in errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}