﻿using Mobilek.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Mobilek.Domain;

namespace Mobilek.WebApi
{
    public interface ICustomerRepository
    {
        IEnumerable<CustomerDTO> GetAll();
        CustomerDTO Find(string id);
        void Update(CustomerDTO item);
        void Add(CustomerDTO item);
    }
    public class CustomerRepository: ICustomerRepository, IDisposable
    {
        private MobilekEntities entities = new MobilekEntities();
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (entities != null)
                    entities.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<CustomerDTO> GetAll()
        {
            return entities.Customers.ProjectTo<CustomerDTO>();
        }
        public CustomerDTO Find(string id)
        {
            int o;
            if(int.TryParse(id,out o))
            {
                return Mapper.Map<CustomerDTO>(entities.Customers.FirstOrDefault(s => s.Id == o));
            }
            else
            {
                return Mapper.Map<CustomerDTO>(entities.Customers.FirstOrDefault(s => s.userName == id));
            }

        }

        public void Update(CustomerDTO item)
        {
            var customer = entities.Customers.FirstOrDefault(s => s.Id == item.Id);
            if(customer!=null)
            {
                customer.firstName = item.firstName;
                customer.creditCardNumber = item.creditCardNumber;
                customer.lastName = item.lastName;
                customer.pesel = item.pesel;
                customer.phoneNumber = item.phoneNumber;
                customer.password = item.password;
                entities.SaveChanges();
            }
        }
        
        public void Add(CustomerDTO item)
        {
            var c = Mapper.Map<Customer>(item);
            entities.Customers.Add(c);
            entities.SaveChanges();
        }
    }
}