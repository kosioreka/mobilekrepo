﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobilek.Helper_Classes
{
    /// <summary>
    /// Class that sets enumaration objects to observable collection
    /// </summary>
    static class ToObservableCollectioncs
    {
        public static ObservableCollection<T> ToObservableCollection<T>(IEnumerable<T> enumeration)
        {
            return new ObservableCollection<T>(enumeration);
        }
    }
}
