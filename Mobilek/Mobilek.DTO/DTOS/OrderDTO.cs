﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobilek.DTO
{
    public class OrderDTO
    {
        public int Id { get; set; }
        public int carsId { get; set; }
        public int customerId { get; set; }
        public System.DateTime startDate { get; set; }
        public Nullable<System.DateTime> endDate { get; set; }
        public Nullable<int> returnStationId { get; set; }
        public int workerId { get; set; }
        public bool isFinished { get; set; }


        public  CustomerDTO Customer { get; set; }
        public StationDTO Station { get; set; }
        public WorkerDTO Worker { get; set; }
        public ICollection<CarDTO> Cars { get; set; }
    }
}
