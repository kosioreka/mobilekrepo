﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mobilek.DTO;
using Mobilek.MVC.Constants;
using Mobilek.MVC.Helpers;
using Mobilek.Services;
using Moq;

namespace Mobilek.MVC.Tests
{
    [TestClass]
    public class AccountHelperTests
    {
        [TestMethod]
        public void RegistryHelperAlreadyInDbTest()
        {
            CustomerDTO c=new CustomerDTO() {userName = "test"};
            var mockService=new Mock<ICustomerDTOService>();

            mockService.Setup(s => s.Get(c.userName)).Returns(Task.FromResult(c));
            string msg;
            var result = AccountHelper.ValidateRegistry(c, mockService.Object, out msg);

            Assert.IsTrue(result==null);
            Assert.IsTrue(msg== Messages.UserExists);
        }

        [TestMethod]
        public void RegistryHelperAddToDBTest()
        {
            CustomerDTO c = new CustomerDTO() { userName = "test" };
            var mockService = new Mock<ICustomerDTOService>();

            mockService.SetupSequence(x => x.Get(c.userName))
            .Returns(Task.FromResult((CustomerDTO)null))
            .Returns(Task.FromResult(c));

            mockService.Setup(x => x.Post(c)).Returns(Task.FromResult(true));

            string msg;
            var result = AccountHelper.ValidateRegistry(c, mockService.Object, out msg);

            Assert.IsTrue(result != null);
            Assert.IsTrue(msg == string.Empty);
        }

        [TestMethod]
        public void LoginHelperCustomerSuccessfullLoginTest()
        {
            var mockCustomerService = new Mock<ICustomerDTOService>();
            var mockWorkerService = new Mock<IWorkerDTOService>();
            string login = "test";
            string password = "test";
            CustomerDTO c=new CustomerDTO() {userName = login,password = SecurePasswordHasher.Hash(password)};

            mockCustomerService.Setup(s => s.Get("test")).Returns(Task.FromResult(c));
            mockWorkerService.Setup(s => s.Get(login)).Returns(Task.FromResult((WorkerDTO) null));

            string msg;
            var result=AccountHelper.ValidateLogin(login, password, mockCustomerService.Object, mockWorkerService.Object, out msg);

            Assert.IsTrue(result!=null);
            Assert.IsTrue(msg==string.Empty);
        }

        [TestMethod]
        public void LoginHelperWorkerSuccessfullLoginTest()
        {
            var mockCustomerService = new Mock<ICustomerDTOService>();
            var mockWorkerService = new Mock<IWorkerDTOService>();
            string login = "test";
            string password = "test";

            WorkerDTO w = new WorkerDTO() {login = login,password = SecurePasswordHasher.Hash(password)};

            mockCustomerService.Setup(s => s.Get("test")).Returns(Task.FromResult((CustomerDTO)null));
            mockWorkerService.Setup(s => s.Get(login)).Returns(Task.FromResult(w));

            string msg;
            var result = AccountHelper.ValidateLogin(login, password, mockCustomerService.Object, mockWorkerService.Object, out msg);

            Assert.IsTrue(result != null);
            Assert.IsTrue(msg == string.Empty);
        }

        [TestMethod]
        public void LoginHelperNotFoundTest()
        {
            var mockCustomerService = new Mock<ICustomerDTOService>();
            var mockWorkerService = new Mock<IWorkerDTOService>();
            string login = "test";
            string password = "test";

            WorkerDTO w = new WorkerDTO() { login = login, password = SecurePasswordHasher.Hash(password) };

            mockCustomerService.Setup(s => s.Get("test")).Returns(Task.FromResult((CustomerDTO)null));
            mockWorkerService.Setup(s => s.Get(login)).Returns(Task.FromResult((WorkerDTO)null));

            string msg;
            var result = AccountHelper.ValidateLogin(login, password, mockCustomerService.Object, mockWorkerService.Object, out msg);

            Assert.IsTrue(result == null);
            Assert.IsTrue(msg != string.Empty);
            Assert.IsTrue(msg==Messages.NoUser);
        }

        [TestMethod]
        public void LoginHelperIncorrectUserPasswordTest()
        {
            var mockCustomerService = new Mock<ICustomerDTOService>();
            var mockWorkerService = new Mock<IWorkerDTOService>();
            string login = "test";
            string password = "test";

            CustomerDTO c = new CustomerDTO() { userName = login, password = SecurePasswordHasher.Hash(password+password) };

            mockCustomerService.Setup(s => s.Get("test")).Returns(Task.FromResult(c));
            mockWorkerService.Setup(s => s.Get(login)).Returns(Task.FromResult((WorkerDTO)null));

            string msg;
            var result = AccountHelper.ValidateLogin(login, password, mockCustomerService.Object, mockWorkerService.Object, out msg);

            Assert.IsTrue(result == null);
            Assert.IsTrue(msg != string.Empty);
            Assert.IsTrue(msg == Messages.IncorrectPassword);
        }
        [TestMethod]
        public void LoginHelperIncorrectAdminPasswordTest()
        {
            var mockCustomerService = new Mock<ICustomerDTOService>();
            var mockWorkerService = new Mock<IWorkerDTOService>();
            string login = "test";
            string password = "test";

            WorkerDTO w = new WorkerDTO() { login = login, password = SecurePasswordHasher.Hash(password+password) };

            mockCustomerService.Setup(s => s.Get("test")).Returns(Task.FromResult((CustomerDTO)null));
            mockWorkerService.Setup(s => s.Get(login)).Returns(Task.FromResult((w)));

            string msg;
            var result = AccountHelper.ValidateLogin(login, password, mockCustomerService.Object, mockWorkerService.Object, out msg);

            Assert.IsTrue(result == null);
            Assert.IsTrue(msg != string.Empty);
            Assert.IsTrue(msg == Messages.IncorrectPassword);
        }

        [TestMethod]
        public void ChangePasswordUserCorrectPasswordTest()
        {
            var mockCustomerService = new Mock<ICustomerDTOService>();
            var mockWorkerService = new Mock<IWorkerDTOService>();
            string login = "test";
            string password = "test";
            CustomerDTO customer=new CustomerDTO() {userName = login,password = SecurePasswordHasher.Hash(password)};


            mockCustomerService.Setup(s => s.Get(login)).Returns(Task.FromResult(customer));
            mockCustomerService.Setup(s => s.Put(customer)).Returns(Task.FromResult(true));

            Claim c = new Claim(ClaimTypes.Role, Roles.UserRole);
            string msg;
            var result = AccountHelper.ValidateChangePassword(c, login, password, password + password,
                mockCustomerService.Object, mockWorkerService.Object, out msg);

            Assert.IsTrue(result);
            Assert.IsTrue(msg==string.Empty);
        }
        [TestMethod]
        public void ChangePasswordAdminCorrectPasswordTest()
        {
            var mockCustomerService = new Mock<ICustomerDTOService>();
            var mockWorkerService = new Mock<IWorkerDTOService>();
            string login = "test";
            string password = "test";
            CustomerDTO customer = new CustomerDTO() { userName = login, password = SecurePasswordHasher.Hash(password) };
            WorkerDTO worker = new WorkerDTO() {login = login, password = SecurePasswordHasher.Hash(password)};

            mockCustomerService.Setup(s => s.Get(login)).Returns(Task.FromResult(customer));
            mockCustomerService.Setup(s => s.Put(customer)).Returns(Task.FromResult(true));

            mockWorkerService.Setup(s => s.Get(login)).Returns(Task.FromResult(worker));
            mockWorkerService.Setup(s => s.Put(worker)).Returns(Task.FromResult(true));

            Claim c = new Claim(ClaimTypes.Role, Roles.AdminRole);
            string msg;
            var result = AccountHelper.ValidateChangePassword(c, login, password, password + password,
                mockCustomerService.Object, mockWorkerService.Object, out msg);

            Assert.IsTrue(result);
            Assert.IsTrue(msg == string.Empty);
        }
        [TestMethod]
        public void ChangePasswordAdminInCorrectPasswordTest()
        {
            var mockCustomerService = new Mock<ICustomerDTOService>();
            var mockWorkerService = new Mock<IWorkerDTOService>();
            string login = "test";
            string password = "test";
            CustomerDTO customer = new CustomerDTO() { userName = login, password = SecurePasswordHasher.Hash(password) };
            WorkerDTO worker = new WorkerDTO() { login = login, password = SecurePasswordHasher.Hash(password+password) };

            mockCustomerService.Setup(s => s.Get(login)).Returns(Task.FromResult(customer));
            mockCustomerService.Setup(s => s.Put(customer)).Returns(Task.FromResult(true));

            mockWorkerService.Setup(s => s.Get(login)).Returns(Task.FromResult(worker));
            mockWorkerService.Setup(s => s.Put(worker)).Returns(Task.FromResult(true));

            Claim c = new Claim(ClaimTypes.Role, Roles.AdminRole);
            string msg;
            var result = AccountHelper.ValidateChangePassword(c, login, password, password + password,
                mockCustomerService.Object, mockWorkerService.Object, out msg);

            Assert.IsTrue(!result);
            Assert.IsTrue(msg != string.Empty);
            Assert.IsTrue(msg==Messages.IncorrectPassword);
        }
        [TestMethod]
        public void ChangePasswordUserInCorrectPasswordTest()
        {
            var mockCustomerService = new Mock<ICustomerDTOService>();
            var mockWorkerService = new Mock<IWorkerDTOService>();
            string login = "test";
            string password = "test";
            CustomerDTO customer = new CustomerDTO() { userName = login, password = SecurePasswordHasher.Hash(password+password) };


            mockCustomerService.Setup(s => s.Get(login)).Returns(Task.FromResult(customer));
            mockCustomerService.Setup(s => s.Put(customer)).Returns(Task.FromResult(true));

            Claim c = new Claim(ClaimTypes.Role, Roles.UserRole);
            string msg;
            var result = AccountHelper.ValidateChangePassword(c, login, password, password + password,
                mockCustomerService.Object, mockWorkerService.Object, out msg);

            Assert.IsTrue(!result);
            Assert.IsTrue(msg != string.Empty);
            Assert.IsTrue(msg==Messages.IncorrectPassword);
        }
        [TestMethod]
        public void ChangePasswordNoClaimTest()
        {
            var mockCustomerService = new Mock<ICustomerDTOService>();
            var mockWorkerService = new Mock<IWorkerDTOService>();
            string login = "test";
            string password = "test";

            Claim c = null; //new Claim(ClaimTypes.Role, Roles.UserRole);
            string msg;
            var result = AccountHelper.ValidateChangePassword(c, login, password, password + password,
                mockCustomerService.Object, mockWorkerService.Object, out msg);

            Assert.IsTrue(!result);
            Assert.IsTrue(msg != string.Empty);
            Assert.IsTrue(msg == Messages.UnexcpectedError);
        }





    }
}
