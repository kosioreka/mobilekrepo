﻿using GalaSoft.MvvmLight.Messaging;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Mobilek.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Mobilek
{
    /// <summary>
    /// The <c>StationAddViewModel</c> defines view for workers's adding a new station functionality
    /// </summary>
    /// <remarks>
    /// In <c>StationAddViewModel</c> a new station object is created
    /// </remarks>
    public class StationAddViewModel : ObservableObject, IPageViewModel, IDataErrorInfo
    {
        #region private fields
        private string city;
        private string street;
        private string zipCode;
        private string streetNo;
        private WorkerDTO w;

        private ICommand acceptCommand;
        private ICommand cancelCommand;
        public Dictionary<string, string> validationErrors = new Dictionary<string, string>();
        #endregion

        #region properties
        public string City
        {
            get { return city; }
            set
            {
                city = value;
                OnPropertyChanged("City");
            }
        }
        public string Street
        {
            get { return street; }
            set
            {
                street = value;
                OnPropertyChanged("Street");
            }
        }
        public string ZipCode
        {
            get { return zipCode; }
            set
            {
                zipCode = value;
                OnPropertyChanged("ZipCode");
            }
        }
        public string StreetNo
        {
            get { return streetNo; }
            set
            {
                streetNo = value;
                OnPropertyChanged("StreetNo");
            }
        }
        public string Name
        {
            get
            {
                return "Station Add";
            }
        }
        #endregion

        #region commands
        public ICommand AcceptCommand
        {
            get
            {
                if (acceptCommand == null)
                {
                    acceptCommand = new RelayCommand(
                        param => Accept());

                }
                return acceptCommand;
            }
        }
        public ICommand CancelCommand
        {
            get
            {
                if (cancelCommand == null)
                {
                    cancelCommand = new RelayCommand(
                        param => Exit());

                }
                return cancelCommand;
            }
        }
        #endregion

        #region methods
        public StationAddViewModel(WorkerDTO w)
            {
            this.w = w;
            }

        private async void Accept()
        {
            Validate();
            if (validationErrors.Count == 0)
            {
                int no;
                if (Int32.TryParse(StreetNo, out no))
                {
                    var sDTO = new StationDTO();

                    sDTO.streetNumer = no;
                    sDTO.city = City;
                    sDTO.street = Street;
                    sDTO.zipCode = zipCode;

                    var service = new Mobilek.Services.StationDTOService(w.login,w.password);
                    await service.Post(sDTO);

                }
                var msg = new FireRefresh();
                Messenger.Default.Send<FireRefresh>(msg);
                Exit();
            }
        }
        private void Validate()
        {
            validationErrors.Clear();
            if (String.IsNullOrWhiteSpace(Street))
            {
                validationErrors.Add("Street", "Type the name of the street!");
            } if (String.IsNullOrWhiteSpace(City))
            {
                validationErrors.Add("City", "Type the name of the city!");
            } if (String.IsNullOrWhiteSpace(StreetNo))
            {
                validationErrors.Add("StreetNo", "Type the street number!");
            } else if (Regex.IsMatch(StreetNo, @"^\d+$") == false)
            {
                validationErrors.Add("StreetNo", "Incorrect street number!");
            } if (String.IsNullOrWhiteSpace(ZipCode))
            {
                validationErrors.Add("ZipCode", "Type the zip code!");
            } else if (Regex.IsMatch(ZipCode, @"^\d{5}$") == false)
            {
                validationErrors.Add("ZipCode", "Wrong zip code! Eg: 94530");
            }
            OnPropertyChanged(null);
        }
        private async void Exit()
        {
            var metroWindow = App.Current.MainWindow as MetroWindow;
            var actualDialog = await metroWindow.GetCurrentDialogAsync<CustomDialog>();
            await metroWindow.HideMetroDialogAsync(actualDialog);
        }
        public string Error
        {
            get
            {
                if (validationErrors.Count > 0)
                {
                    return "Errors found.";
                }
                return null;
            }
        }
        public string this[string columnName]
        {
            get
            {
                if (validationErrors.ContainsKey(columnName))
                {
                    return validationErrors[columnName];
                }
                return null;
            }
        }
        #endregion
    }
}
