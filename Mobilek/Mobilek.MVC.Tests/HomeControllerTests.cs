﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mobilek.MVC.Controllers;
using System.Web.Mvc;

namespace Mobilek.MVC.Tests
{
    [TestClass]
    public class HomeControllerTests
    {
        [TestMethod]
        public void HomeIndexTest()
        {
            var controller = new HomeController();

            var result = controller.Index();

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result,typeof(ActionResult));


        }
    }
}
