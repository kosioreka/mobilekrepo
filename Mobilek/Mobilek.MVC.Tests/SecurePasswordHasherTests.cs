﻿using System;
using System.Reflection.Emit;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mobilek.MVC.Helpers;

namespace Mobilek.MVC.Tests
{
    [TestClass]
    public class SecurePasswordHasherTests
    {
        public string TestString { get; set; }
        public int IterNumber { get; set; }

        [TestInitialize]
        public void Init()
        {
            TestString = "teststring";
            IterNumber = 50;
        }


        [TestMethod]
        public void HashNaiveTest()
        {

            var result =SecurePasswordHasher.Hash(TestString);

            Assert.IsTrue(TestString!=result);
        }

        [TestMethod]
        public void HashMultiIterationTest()
        {
            string[] array=new string[IterNumber - 1];
            for (int i = 1; i < IterNumber; i++)
            {
                array[i-1]= SecurePasswordHasher.Hash(TestString, i);
            }

            for (int i = 0; i < array.Length - 1; i++)
            {
                Assert.IsTrue(array[i]!=array[i+1]);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void HashZeroIterationExceptionTest()
        {
            SecurePasswordHasher.Hash(TestString, 0);
        }

        [TestMethod]
        public void HashVerifyPositiveTest()
        {
            var hash= SecurePasswordHasher.Hash(TestString);

            Assert.IsTrue(SecurePasswordHasher.Verify(TestString,hash));
        }

        [TestMethod]
        public void HashMultiIterationVerifyPositiveTest()
        {
            for (int i = 1; i < IterNumber; i++)
            {
                var hash = SecurePasswordHasher.Hash(
                    TestString, i);
                Assert.IsTrue(SecurePasswordHasher.Verify(TestString, hash));
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public void HashVerifyNonSupportedExceptionNegativeTest()
        {
            var hash = SecurePasswordHasher.Hash(TestString);

            Assert.IsTrue(SecurePasswordHasher.Verify(TestString,TestString));
        }

        [TestMethod]
        public void HashVerifyDiffrentStringTest()
        {
            var hash = SecurePasswordHasher.Hash(TestString);

            Assert.IsFalse(SecurePasswordHasher.Verify(TestString+TestString,hash));
        }

        [TestMethod]
        public void HashIsSupportedTest()
        {
            var hash = SecurePasswordHasher.Hash(TestString);

            Assert.IsTrue(SecurePasswordHasher.IsHashSupported(hash));
            Assert.IsFalse(SecurePasswordHasher.IsHashSupported(TestString));
        }


    }
}
