﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Mobilek.DTO;

namespace Mobilek.WebApi.Controllers
{
    [Authorize]

    public class WorkerController : ApiController
    {
        readonly IWorkerRepository _workerRepository;
        public WorkerController(IWorkerRepository repo)
        {
            _workerRepository = repo;
        }
        
        public IHttpActionResult Get()
        {
            return this.Ok(_workerRepository.GetAll());
        }
        
        // GET: api/Station/5
        public IHttpActionResult Get(string id)
        {
            WorkerDTO worker = _workerRepository.Find(id);
            if (worker == null)
            {
                return NotFound();
            }
            return this.Ok(worker);
        }

        [Authorize]
        public IHttpActionResult Put([FromBody]WorkerDTO value)
        {
            _workerRepository.Update(value);
            return this.Ok(true);
        }
    }
}
