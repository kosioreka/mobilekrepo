﻿using GalaSoft.MvvmLight.Messaging;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Mobilek.DTO;
using Mobilek.Helper_Classes;
using Mobilek.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;

namespace Mobilek
{
    /// <summary>
    /// The <c>CarAddViewModel</c> defines view for workers's adding a new car functionality
    /// </summary>
    /// <remarks>
    /// In <c>CarAddViewModel</c> a new car object is created
    /// </remarks>
    public class CarAddViewModel : ObservableObject, IPageViewModel, IDataErrorInfo
    {
        #region private fields
        private string model;
        private string colour;
        private DateTime productionDate;
        private decimal price;
        private bool isEfficient;
        private WorkerDTO w;

        ObservableCollection<StationDTO> _stationsCollection = new ObservableCollection<StationDTO>();
        private StationDTO station;
        ObservableCollection<string> _brandCollection = new ObservableCollection<string>();
        private string _selectedBrand;
        ObservableCollection<CarFromApi> _modelCollection = new ObservableCollection<CarFromApi>();
        private CarFromApi _selectedModel;
        public Dictionary<string, string> validationErrors = new Dictionary<string, string>();

        private ICommand acceptCommand;
        private ICommand cancelCommand;

        private FrenchApi frenchApi = new FrenchApi();
        #endregion

        #region properties
        public string Model
        {
            get { return model; }
            set
            {
                model = value;
                OnPropertyChanged("Model");
            }
        }
        public string Colour
        {
            get { return colour; }
            set
            {
                string[] words = value.Split(' ');
                if (words.Length == 2)
                {
                    colour = words[1];
                OnPropertyChanged("Colour");
            }
        }
        }
        public DateTime ProductionDate
        {
            get { return productionDate; }
            set
            {
                productionDate = value;
                OnPropertyChanged("ProductionDate");
            }
        }
        public decimal Price
        {
            get { return price; }
            set
            {
                price = value;
                OnPropertyChanged("Price");
            }
        }
        public bool IsEfficient
        {
            get { return isEfficient; }
            set
            {
                isEfficient = value;
                OnPropertyChanged("IsEfficient");
            }
        }
        public StationDTO Station
        {
            get { return station; }
            set
            {
                station = value;
                OnPropertyChanged("Station");
            }
        }

        public ObservableCollection<StationDTO> StationsCollection
        {
            get
            {
                return _stationsCollection;
            }
            set
            {
                _stationsCollection = value;
                OnPropertyChanged("StationsCollection");
            }
        }
        public string SelectedBrand
        {
            get { return _selectedBrand; }
            set
            {
                _selectedBrand = value;
                OnPropertyChanged("SelectedBrand");
                ModelCollection = ToObservableCollectioncs.ToObservableCollection(FrenchApi.GetCarsByBrand(SelectedBrand));
            }
        }
        public ObservableCollection<string> BrandCollection
        {
            get
            {
                return _brandCollection;
            }
            set
            {
                _brandCollection = value;
                OnPropertyChanged("BrandCollection");
            }
        }
        public CarFromApi SelectedModel
        {
            get { return _selectedModel; }
            set
            {
                _selectedModel = value;
                if (_selectedModel != null)
                {
                    Model = _selectedModel.modele_dossier;
                }
                else
                {
                    Model = null;
                }
                OnPropertyChanged("SelectedModel");
            }
        }
        public ObservableCollection<CarFromApi> ModelCollection
        {
            get
            {
                return _modelCollection;
            }
            set
            {
                _modelCollection = value;
                OnPropertyChanged("ModelCollection");
            }
        }

        public string Name
        {
            get
            {
                return "Car add";
            }
        }
        #endregion


        #region commands
        public ICommand AcceptCommand
        {
            get
            {
                if (acceptCommand == null)
                {
                    acceptCommand = new RelayCommand(
                        param => Accept());

                }
                return acceptCommand;
            }
        }
        public ICommand CancelCommand
        {
            get
            {
                if (cancelCommand == null)
                {
                    cancelCommand = new RelayCommand(
                        param => Exit());

                }
                return cancelCommand;
            }
        }
        #endregion

        #region methods
        public CarAddViewModel(WorkerDTO w)
        {
            this.w = w;
            GetStations();
            ProductionDate = new DateTime(2000, 01, 01);
            GetUniqueBrands();
        }
        private async void GetUniqueBrands()
        {
            var brandNames = await FrenchApi.GetUniqueBrandCarsAsync();
            BrandCollection = ToObservableCollectioncs.ToObservableCollection<string>(brandNames);
        }
        private async void GetStations()
        {
            var service = new StationDTOService(w.login,w.password);
            var stationss = Task.Run(() =>  service.Get()).Result;
            var stations = stationss.AsEnumerable();

            StationsCollection = ToObservableCollectioncs.ToObservableCollection<StationDTO>(stations);
        }

        private async void Accept()
        {
            Validate();
            if (validationErrors.Count == 0)
            {
                var c = new CarDTO();
                c.stationId = Station.Id;
                c.brand = SelectedBrand;
                c.colour = Colour;
                c.isEfficient = IsEfficient;
                c.model = Model;
                c.price = Price;
                c.productionDate = ProductionDate;
                c.frenchId = SelectedModel.recordid;

                var service = new CarDTOService(w.login, w.password);
                await service.Post(c);

                var msg = new FireRefresh();
                Messenger.Default.Send<FireRefresh>(msg);
                Exit();
            }
        }
        public void Validate()
        {
            validationErrors.Clear();
            if (String.IsNullOrWhiteSpace(Model))
            {
                validationErrors.Add("SelectedModel", "Choose model of the car!");
            } if (String.IsNullOrWhiteSpace(SelectedBrand))
            {
                validationErrors.Add("SelectedBrand", "Choose brand of the car!");
            } if (String.IsNullOrWhiteSpace(Colour))
            {
                validationErrors.Add("Colour", "Choose colour of the car!");
            } if (Price == 0)
            {
                validationErrors.Add("Price", "Incorrect price!");
            } if (Station == null)
            {
                validationErrors.Add("Station", "Choose station of the car!");
            }
            OnPropertyChanged(null);
        }
        private async void Exit()
        {
            var metroWindow = App.Current.MainWindow as MetroWindow;
            var actualDialog = await metroWindow.GetCurrentDialogAsync<CustomDialog>();
            await metroWindow.HideMetroDialogAsync(actualDialog);
        }
        #endregion


        public string Error
        {
            get
            {
                if (validationErrors.Count > 0)
                {
                    return "Errors found.";
                }
                return null;
            }
        }

        public string this[string columnName]
        {
            get
            {
                if (validationErrors.ContainsKey(columnName))
                {
                    return validationErrors[columnName];
                }
                return null;
            }
        }

    }
}
