﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Practices;
using GalaSoft.MvvmLight.Messaging;

namespace Mobilek
{
    /// <summary>
    /// The <c>MainWindowViewModel</c> is the main window
    /// Difined views are shown on the main window
    /// </summary>
    public class MainWindowViewModel : ObservableObject
    {
        #region Fields

        private ICommand _changePageCommand;

        private IPageViewModel _currentPageViewModel;
        private List<IPageViewModel> _pageViewModels;
        private List<IPageViewModel> _customerViewModels;
        private List<IPageViewModel> _shownViewModels;        

        #endregion

        public MainWindowViewModel()
        {
            PageViewModels.Add(new WelcomeViewModel());

            ShownViewModels.Add(new WelcomeViewModel());
            CurrentPageViewModel = PageViewModels[0];

            Messenger.Default.Register<ChangePageHelper>
            (
                 this,
                 (action) => ChangeViewModel(action.PageName)
            );


        }

        #region Properties / Commands

        public ICommand ChangePageCommand
        {
            get
            {
                if (_changePageCommand == null)
                {
                    _changePageCommand = new RelayCommand(
                        p => ChangeViewModel((IPageViewModel)p),
                        p => p is IPageViewModel);
                }

                return _changePageCommand;
            }
        }

        public List<IPageViewModel> PageViewModels
        {
            get
            {
                if (_pageViewModels == null)
                    _pageViewModels = new List<IPageViewModel>();

                return _pageViewModels;
            }
        }
        public List<IPageViewModel> CustomerViewModels
        {
            get
            {
                if (_customerViewModels == null)
                    _customerViewModels = new List<IPageViewModel>();

                return _customerViewModels;
            }
        }
        public List<IPageViewModel> ShownViewModels
        {
            get
            {
                if (_shownViewModels == null)
                    _shownViewModels = new List<IPageViewModel>();

                return _shownViewModels;
            }
        }
        public IPageViewModel CurrentPageViewModel
        {
            get
            {
                return _currentPageViewModel;
            }
            set
            {
                if (_currentPageViewModel != value)
                {
                    _currentPageViewModel = value;
                    OnPropertyChanged("CurrentPageViewModel");
                }
            }
        }

        #endregion

        #region Methods

        private void ChangeViewModel(IPageViewModel viewModel)
        {
            if (!PageViewModels.Contains(viewModel))
                PageViewModels.Add(viewModel);

            CurrentPageViewModel = PageViewModels
                .FirstOrDefault(vm => vm == viewModel);
        }


        #endregion
    }
}
