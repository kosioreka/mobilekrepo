﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Mobilek.DTO;
using Mobilek.WebApi.Controllers;
using System.Web.Http;
using System.Web.Http.Results;
using Mobilek.Domain;
using AutoMapper;


namespace Mobilek.WebApi.Tests
{
    [TestClass]
    public class CarControllerTest
    {
        [TestInitialize]
        public void testInit()
        {
            Mobilek.WebApi.AutomapperConfigure.Configure();
        }
        [TestMethod]
        public void GetAllCars_Test()
        {

            var mockRepo = new Mock<ICarRepository>();
            mockRepo.Setup(s => s.GetAll()).Returns(new List<CarDTO>() { new CarDTO() });

            var controller = new CarController(mockRepo.Object);

            var getallResult = controller.Get();

            Assert.IsInstanceOfType(getallResult, typeof(OkNegotiatedContentResult<IEnumerable<CarDTO>>));

            var or = getallResult as OkNegotiatedContentResult<IEnumerable<CarDTO>>;
            Assert.IsTrue(or.Content.Count() == 1);

        }
        [TestMethod]
        public void FindByIdCar_Test()
        {
            var mockRepo = new Mock<ICarRepository>();
            int id = 5;
            int noid = 33;

            mockRepo.Setup(s => s.Find(5)).Returns(new CarDTO() { Id = id });

            var controller = new CarController(mockRepo.Object);

            var getallResult = controller.Get(id);

            Assert.IsInstanceOfType(getallResult, typeof(OkNegotiatedContentResult<CarDTO>));

            var worker = getallResult as OkNegotiatedContentResult<CarDTO>;

            Assert.IsTrue(worker.Content.Id == id);

            var getresults2 = controller.Get(noid);
            Assert.IsInstanceOfType(getresults2, typeof(NotFoundResult));

        }
        [TestMethod]
        public void GetReturnsCarByBrandTest()
        {
            string brand = "Car";
            string noBrand = "noCar";

            // Arrange
            var mockRepository = new Mock<ICarRepository>();
            mockRepository.Setup(x => x.FindByBrand(brand)).Returns(new List<CarDTO>() { new CarDTO()});
            var controller = new CarController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.GetBrand(brand);
            var createdResult = actionResult as OkNegotiatedContentResult<IEnumerable<CarDTO>>;
            var createdResult2 = actionResult as CreatedAtRouteNegotiatedContentResult<IEnumerable<CarDTO>>;

            // Assert
            Assert.IsNotNull(createdResult.Content);
            // Assert
            Assert.IsNotNull(actionResult);

            IHttpActionResult noActionResult = controller.GetBrand(noBrand);
            Assert.IsInstanceOfType(noActionResult, typeof(NotFoundResult));
        }
    }
}
