﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Mobilek.DTO;
using Mobilek.Helper_Classes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Mobilek
{
    /// <summary>
    /// The <c>CarsOrderViewModel</c> defines dialog for choosing cars in a user order
    /// </summary>
    /// <remarks>
    /// The starting station is required
    /// User chooses from one to many cars from the starting station
    /// Returns the collection of chosen cars
    /// </remarks>
    class CarsOrderViewModel : ObservableObject, IPageViewModel
    {
        #region private fields
        private ICommand _goBack;
        private ICommand _carsSearch;
        private ICommand _addCar;

        private string _order_carBrand;
        private string _order_carModel;
        private string _order_carColour;
        private decimal _order_carLowestPrice;
        private decimal _order_carHeighestPrice;
        private bool _order_carIsAvailable;
        private StationDTO station;
        private CustomerDTO w;
        private ObservableCollection<CarDTO> _carCollection = new ObservableCollection<CarDTO>();
        private Collection<CarDTO> carList = new Collection<CarDTO>();
        private int _carAmount;
        #endregion

        #region properties
        public const string SelectedCarsPropertyName = "SelectedCars";
        public RelayCommand<Collection<CarDTO>> SendCarCollectionCommand { get; set; }

        public int CarAmount { 
            get { return _carAmount; } 
            set { _carAmount = value; 
                OnPropertyChanged("CarAmount"); 
            }
        }

        public CarDTO SelectedCar { get; set; } 
        public ObservableCollection<CarDTO> CarsCollection
        {
            get
            {
                return _carCollection;
            }
            set
            {
                _carCollection = value;
                OnPropertyChanged("CarsCollection");
            }
        }

        public CarsOrderViewModel(StationDTO station,CustomerDTO w)
        {
            this.station = station;
            this.w = w;
            SearchCars();
            CarAmount = 0;
        }
        public string Order_CarBrand
        {
            get { return _order_carBrand; }
            set { _order_carBrand = value;
            OnPropertyChanged("Order_CarBrand");
            }
        }
        public string Order_CarModel
        {
            get { return _order_carModel; }
            set
            {
                _order_carModel = value;
                OnPropertyChanged("Order_CarModel");
            }
        }
        public string Order_CarColour
        {
            get { return _order_carColour; }
            set
            {
                _order_carColour = value;
                OnPropertyChanged("Order_CarColour");
            }
        }
        public decimal Order_CarLowestPrice
        {
            get { return _order_carLowestPrice; }
            set
            {
                _order_carLowestPrice = value;
                OnPropertyChanged("Order_CarLowestPrice");
            }
        }
        public decimal Order_CarHeighestPrice
        {
            get { return _order_carHeighestPrice; }
            set
            {
                _order_carHeighestPrice = value;
                OnPropertyChanged("Order_CarHeighestPrice");
            }
        }
        public bool Order_CarIsAvailable
        {
            get { return _order_carIsAvailable; }
            set
            {
                _order_carIsAvailable = value;
                OnPropertyChanged("Order_CarIsAvailable");
            }
        }
        public string Name
        {
            get { return "Cars Order"; }
        }
        #endregion

        #region Commands
        public ICommand GoBack
        {
            get
            {
                if (_goBack == null)
                {
                    _goBack = new RelayCommand(
                        param => GoBackToOrderView());

                }
                return _goBack;
            }
        }
        public ICommand CarsSearch
        {
            get
            {
                if (_carsSearch == null)
                {
                    _carsSearch = new RelayCommand(
                        param => SearchCars());

                }
                return _carsSearch;
            }
        }
        public ICommand AddCar
        {
            get
            {
                if (_addCar == null)
                {
                    _addCar = new RelayCommand(
                        param => AddCars());

                }                
                return _addCar;
            }
        }
        #endregion 

        #region Methods
        private async void SearchCars()
        {
            {
                var service = new Mobilek.Services.CarDTOService(w.userName,w.password);
                var cars = await service.Get();
                var carList = cars.AsEnumerable();

                if(station!=null)
                    carList = carList.Where(c => c.stationId.Equals(station.Id));
                if (!string.IsNullOrEmpty(_order_carBrand))
                {
                    carList = carList.Where(s => s.brand.StartsWith(_order_carBrand));
                } 
                if (!string.IsNullOrEmpty(_order_carModel))
                {
                    carList = carList.Where(s => s.model.StartsWith(_order_carBrand));
                } 
                if (!string.IsNullOrEmpty(_order_carColour))
                {
                    carList = carList.Where(s => s.colour.StartsWith(_order_carColour));
                } 
                if (_order_carLowestPrice>0)
                {
                    carList = carList.Where(s => s.price >= _order_carLowestPrice);
                }
                if (_order_carHeighestPrice > 0 && _order_carHeighestPrice<_order_carLowestPrice)
                {
                    carList = carList.Where(s => s.price <= _order_carHeighestPrice);
                }
                if (!_order_carIsAvailable)
                {
                    //to do
                }
                CarsCollection = ToObservableCollectioncs.ToObservableCollection<CarDTO>(carList);

            }
        }
        private void AddCars()
        {
            if (SelectedCar != null && !carList.Contains(SelectedCar))
            {
                carList.Add(SelectedCar);
                CarAmount++;
            }
        }
#pragma warning disable CS1998 // This async method lacks 'await' operators and will run synchronously. Consider using the 'await' operator to await non-blocking API calls, or 'await Task.Run(...)' to do CPU-bound work on a background thread.
        private async void GoBackToOrderView()
#pragma warning restore CS1998 // This async method lacks 'await' operators and will run synchronously. Consider using the 'await' operator to await non-blocking API calls, or 'await Task.Run(...)' to do CPU-bound work on a background thread.
        {

            var msg = new FireRefresh();
            Messenger.Default.Send<FireRefresh>(msg);
            SendCarCollectionInfo();
            Exit();
        }
        private void SendCarCollectionInfo()
        {
            if (carList.Count != 0 && carList != null)
            {
                Messenger.Default.Send<MessageCommunicatorNewOrder>(new MessageCommunicatorNewOrder()
                {
                    carsCollection = carList,
                    submittedOrder = false
                   
                });
            }
        }
        private async void Exit()
        {
            var metroWindow = App.Current.MainWindow as MetroWindow;
            var actualDialog = await metroWindow.GetCurrentDialogAsync<CustomDialog>();
            await metroWindow.HideMetroDialogAsync(actualDialog);
        }
        #endregion
 
    }
}
