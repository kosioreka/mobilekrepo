﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobilek.DTO
{
    public class CarDTO
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Brand")]
        public string brand { get; set; }
        [Required]
        [Display(Name = "Model")]
        public string model { get; set; }
        [Required]
        [Display(Name = "Colour")]
        public string colour { get; set; }
        public System.DateTime productionDate { get; set; }
        public int stationId { get; set; }
        [DataType(DataType.Currency)]
        public decimal price { get; set; }
        public bool isEfficient { get; set; }
        public string frenchId { get; set; }

        //public ICollection<OrderDTO> Orders { get; set; }
        //public StationDTO Station { get; set; }

        public string stationName { get; set; }
    }
}
