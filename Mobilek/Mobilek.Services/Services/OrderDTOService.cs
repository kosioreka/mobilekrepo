﻿using Mobilek.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Mobilek.Services
{
    public interface IOrderDTOService
    {
        Task<OrderDTO> Get(int id);

        Task<IList<OrderDTO>> Get();

        Task<OrderDTO> Post(OrderDTO item);

    }
    public class OrderDTOService : WebApiService,IOrderDTOService
    {
        public OrderDTOService(string l,string p) : base("Order")
        {
            this._httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue(
                    "Basic",
                    Convert.ToBase64String(
                        System.Text.ASCIIEncoding.ASCII.GetBytes(
                            string.Format("{0}:{1}", l, p))));
        }

        public async Task<OrderDTO> Get(int id)
        {
            return await this.Get<OrderDTO>(id.ToString());
        }
        public async Task<IList<OrderDTO>> Get()
        {
            return await this.Get<IList<OrderDTO>>("");
        }
        public async Task<OrderDTO> Post(OrderDTO item)
        {
            return await this.Post<OrderDTO, OrderDTO>("POST", item);
        }
    }
}
