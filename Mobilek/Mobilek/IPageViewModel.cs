﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mobilek
{
    /// <summary>
    /// The <c>IPageViewModel</c> defines interface for every view in the project
    /// Every class that inherits from that interface must have a name field
    /// </summary>
    public interface IPageViewModel
    {
        string Name { get; }
    }
}
