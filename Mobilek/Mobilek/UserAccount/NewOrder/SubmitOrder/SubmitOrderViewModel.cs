﻿using GalaSoft.MvvmLight.Messaging;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Mobilek
{
    /// <summary>
    /// The <c>SubmitOrderViewModel</c> defines view (dialog) for order submitting confirmation
    /// </summary>
    /// <remarks>
    /// The order can be submitted or canceled
    /// </remarks>
    class SubmitOrderViewModel: IPageViewModel
    {
        
        private ICommand _submit;
        private ICommand _cancel;

        #region commands
        public ICommand submit
        {
            get
            {
                if (_submit == null)
                {
                    _submit = new RelayCommand(
                        param => GoBackToOrderView());

                }
                //submitted = true;
                return _submit;
            }
        }
        public ICommand cancel
        {
            get
            {
                if (_cancel == null)
                {
                    _cancel = new RelayCommand(
                        param => Exit());

                }
                return _cancel;
            }
        }
        #endregion

        #region methods, properties

#pragma warning disable CS1998 // This async method lacks 'await' operators and will run synchronously. Consider using the 'await' operator to await non-blocking API calls, or 'await Task.Run(...)' to do CPU-bound work on a background thread.
        private async void GoBackToOrderView()
#pragma warning restore CS1998 // This async method lacks 'await' operators and will run synchronously. Consider using the 'await' operator to await non-blocking API calls, or 'await Task.Run(...)' to do CPU-bound work on a background thread.
        {
            var msg = new FireRefresh();
            Messenger.Default.Send<FireRefresh>(msg);
            SendSubmitInfo();
            Exit();
        }
        private void SendSubmitInfo()
        {

                Messenger.Default.Send<MessageCommunicatorNewOrder>(new MessageCommunicatorNewOrder()
                {
                    submittedOrder = true
                });

        }
        private async void Exit()
        {
            var metroWindow = App.Current.MainWindow as MetroWindow;
            var actualDialog = await metroWindow.GetCurrentDialogAsync<CustomDialog>();
            await metroWindow.HideMetroDialogAsync(actualDialog);
        }

        public string Name
        {
            get { return "Submit Dialog"; }
        }
        #endregion
    }
}
