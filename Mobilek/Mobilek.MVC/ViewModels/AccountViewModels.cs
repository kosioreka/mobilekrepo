﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mobilek.MVC.ViewModels
{

    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Login")]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }



    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} must be {2} long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("password", ErrorMessage = "Passwords must be the same.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} musi składać się z minimum {2} znaków.", MinimumLength = 1)]
        [Display(Name = "Name")]
        public string firstName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} must be {2} long.", MinimumLength = 1)]
        [Display(Name = "Last name")]
        public string lastName { get; set; }
        [Required]
        [RegularExpression(@"[0-9]{11}", ErrorMessage = "Incorrect {0}")]
        [Display(Name = "Pesel")]
        public string pesel { get; set; }
        [Required]
        [Phone(ErrorMessage = "Invalid {0}")]
        [Display(Name = "Phone Nr")]
        public string phoneNumber { get; set; }

        [RegularExpression(@"^\d{0,16}$", ErrorMessage = "Incorrecty {0}")]
        [Display(Name = "Credit card number")]
        public string creditCardNumber { get; set; }

        [StringLength(100, ErrorMessage = "{0} must be {2} long.", MinimumLength = 2)]
        [Display(Name = "Login")]
        public string userName { get; set; }

    }
    public class EditProfileViewModel
    {
        public string password { get; set; }
        public int LastTabIndex { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} musi składać się z minimum {2} znaków.", MinimumLength = 1)]
        [Display(Name = "Name")]
        public string firstName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} must be {2} long.", MinimumLength = 1)]
        [Display(Name = "Last name")]
        public string lastName { get; set; }
        [Required]
        [RegularExpression(@"[0-9]{11}", ErrorMessage = "Incorrect {0}")]
        [Display(Name = "Pesel")]
        public string pesel { get; set; }
        [Required]
        [Phone(ErrorMessage = "Invalid {0}")]
        [Display(Name = "Phone Nr")]
        public string phoneNumber { get; set; }

        [RegularExpression(@"^\d{0,16}$", ErrorMessage = "Incorrecty {0}")]
        [Display(Name = "Credit card number")]
        public string creditCardNumber { get; set; }

        [StringLength(100, ErrorMessage = "{0} must be {2} long.", MinimumLength = 2)]
        [Display(Name = "Login")]
        public string userName { get; set; }

        public bool IsEdited { get; set; }

    }
    public class ResetPasswordViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "{0} must be {2} long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Old password")]
        public string OldPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        [StringLength(100, ErrorMessage = "{0} must be {2} long.", MinimumLength = 6)]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "Passwords must be the same.")]
        public string ConfirmPassword { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

}