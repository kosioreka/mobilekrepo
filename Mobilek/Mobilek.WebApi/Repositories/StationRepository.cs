﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mobilek.DTO;
using AutoMapper.QueryableExtensions;
using AutoMapper;
using Mobilek.Domain;


namespace Mobilek.WebApi
{
    public interface IStationRepository
    {
        void Add(StationDTO item);
        IEnumerable<StationDTO> GetAll();
        StationDTO Find(int id);
        IEnumerable<StationDTO> FindByCity(string city);
        bool Remove(int id);
        void Update(StationDTO item);
    }
    public class StationRepository : IStationRepository,IDisposable
    {
        private MobilekEntities entities = new MobilekEntities();

        public void Add(StationDTO item)
        {
            entities.Stations.Add(Mapper.Map<Station>(item));
            entities.SaveChanges();
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if(entities!= null)
                    entities.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public StationDTO Find(int id)
        {
            //TODO
            var station = entities.Stations.FirstOrDefault(s => s.Id == id);
            if(station== null)
            {
                return Mapper.Map<StationDTO>(station);
            }
            else
            {
                return Mapper.Map<StationDTO>(station);
            }
            
        }
        public IEnumerable<StationDTO> FindByCity(string city)
        {
            return entities.Stations.OrderBy(c => c.street).Where(c => c.city == city).ProjectTo<StationDTO>();
        }

        public IEnumerable<StationDTO> GetAll()
        {
            var k= entities.Stations.ProjectTo<StationDTO>();
            return k;
        }

        public bool Remove(int id)
        {
            var st = entities.Stations.FirstOrDefault(s => s.Id== id);
            if (st != null)
            {
                if(st.Orders.Count==0 && st.Cars.Count==0)
                {
                    entities.Stations.Remove(st);
                    entities.SaveChanges();
                    return true;
                }
            }
            return false;
        }

        public void Update(StationDTO item)
        {

            var st = entities.Stations.FirstOrDefault(s => s.Id == item.Id);
            if (st != null)
            {
                st.city = item.city;
                st.street = item.street;
                st.streetNumer = item.streetNumer;
                st.zipCode = item.zipCode;
                entities.SaveChanges();
            }
        }

    }
}