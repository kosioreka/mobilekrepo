﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Mobilek.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mobilek.Domain;

namespace Mobilek.WebApi
{
    public interface IOrderRepository
    {
        void Add(OrderDTO item);
        IEnumerable<OrderDTO> GetAll();
        OrderDTO Find(int id);
        //void Remove(int id);
        //void Update(OrderDTO item);
    }
    public class OrderRepository: IOrderRepository, IDisposable
    {
        private MobilekEntities entities = new MobilekEntities();

        public void Add(OrderDTO item)
        {
            entities.Orders.Add(Mapper.Map<Order>(item));
            entities.SaveChanges();
        }
        public OrderDTO Find(int id)
        {
            var order = entities.Orders.FirstOrDefault(s => s.Id == id);
            return Mapper.Map<OrderDTO>(order);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (entities != null)
                    entities.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        IEnumerable<OrderDTO> IOrderRepository.GetAll()
        {
            var orders = entities.Orders;
            var ordersDTO = Mapper.Map<IEnumerable<OrderDTO>>(orders);
            return ordersDTO;
            //return entities.Orders.ProjectTo<OrderDTO>();
        }
    }
}