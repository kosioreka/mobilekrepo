﻿using GalaSoft.MvvmLight.Messaging;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Mobilek.DTO;
using Mobilek.Helper_Classes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Mobilek
{
    /// <summary>
    /// The <c>StationOrderViewModel</c> defines view (dialog) for choosing station in a user order
    /// </summary>
    /// <remarks>
    /// It's used to choose starting station and returning station seperatly
    /// It returns either starting station or returning station
    /// </remarks>
    class StationOrderViewModel : ObservableObject, IPageViewModel
    {
        #region private fields
        private ICommand _findStations;
        private ICommand _goBack;
        private CustomerDTO w;
        ObservableCollection<StationDTO> _stationCollection = new ObservableCollection<StationDTO>();
        private string _order_street;
        private string _order_city;
        private string _order_streetNumber;
        private string _order_zipCode;
        private string stationType;
        private CustomerDTO customer;
        #endregion

        #region properties
        public StationDTO SelectedStation { get; set; }
        public string Order_Street
        {
            get { return _order_street; }
            set
            {
                _order_street = value;
                OnPropertyChanged("Order_Street");
            }
        }
        public string Order_City
        {
            get { return _order_city; }
            set
            {
                _order_city = value;
                OnPropertyChanged("Order_City");
            }
        }
        public string Order_StreetNumber
        {
            get { return _order_streetNumber; }
            set
            {
                _order_streetNumber = value;
                OnPropertyChanged("Order_StreetNumber");
            }
        }
        public string Order_ZipCode
        {
            get { return _order_zipCode; }
            set
            {
                _order_zipCode = value;
                OnPropertyChanged("Order_ZipCode");
            }
        }
        public ObservableCollection<StationDTO> StationCollection
        {
            get { return _stationCollection; }
            set { _stationCollection = value; OnPropertyChanged("StationCollection"); }
        }
        public string Name
        {
            get { return "Station Order"; }
        }
        #endregion

        #region methods
        private async void GetStations()
        {
            //using (var entities = new MobilekEntities())
            {
                var service = new Mobilek.Services.StationDTOService(w.userName,w.password);
                var stations = await service.Get();
                var stationList = stations.AsEnumerable();
                //var stationList = entities.Stations.AsEnumerable();
                if (!string.IsNullOrEmpty(_order_city))
                {
                    stationList = stationList.Where(s => s.city.StartsWith(_order_city));
                } if (!string.IsNullOrEmpty(_order_street))
                {
                    stationList = stationList.Where(s => s.street.StartsWith(_order_street));
                } if (!string.IsNullOrEmpty(_order_streetNumber))
                {
                    stationList = stationList.Where(s => s.streetNumer.Equals(_order_streetNumber));
                } if (!string.IsNullOrEmpty(_order_zipCode))
                {
                    stationList = stationList.Where(s => s.zipCode.StartsWith(_order_zipCode));
                }
                StationCollection = ToObservableCollectioncs.ToObservableCollection<StationDTO>(stationList);
            }
            }
        private void SendStationInfo()
        {
            if (SelectedStation != null)
            {
                if (stationType == "StartStation")
                {
                    Messenger.Default.Send<MessageCommunicatorNewOrder>(new MessageCommunicatorNewOrder()
                    {
                        startingStation = SelectedStation,
                        customer = customer
                    });
                }
                else
                {
                    Messenger.Default.Send<MessageCommunicatorNewOrder>(new MessageCommunicatorNewOrder()
                    {
                        endingStation = SelectedStation,
                        customer = customer
                    });
                }
            }
        }
#pragma warning disable CS1998 // This async method lacks 'await' operators and will run synchronously. Consider using the 'await' operator to await non-blocking API calls, or 'await Task.Run(...)' to do CPU-bound work on a background thread.
        private async void GoBackToOrderViewMethod()
#pragma warning restore CS1998 // This async method lacks 'await' operators and will run synchronously. Consider using the 'await' operator to await non-blocking API calls, or 'await Task.Run(...)' to do CPU-bound work on a background thread.
        {
            var msg = new FireRefresh();
            Messenger.Default.Send<FireRefresh>(msg);
            SendStationInfo();
            Exit();
        }
        private async void Exit()
        {
            var metroWindow = App.Current.MainWindow as MetroWindow;
            var actualDialog = await metroWindow.GetCurrentDialogAsync<CustomDialog>();
            await metroWindow.HideMetroDialogAsync(actualDialog);
        }
        public StationOrderViewModel(string stationType, CustomerDTO customer)
        {
            w = customer;
            GetStations();
            this.stationType = stationType;
            this.customer = customer;
        }
        #endregion

        #region commands
        public ICommand FindStations
        {
            get
            {
                if (_findStations == null)
                {
                    _findStations = new RelayCommand(
                        param => GetStations());

                }
                return _findStations;
            }
        }
        public ICommand GoBackToOrderView
        {
            get
            {
                if (_goBack == null)
        {
                    _goBack = new RelayCommand(
                        param => GoBackToOrderViewMethod());

                }
                return _goBack;
            }
        }
        #endregion 
    }
}
