﻿using Mobilek.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Mobilek.WebApi.Controllers
{
    [Authorize]
    public class OrderController : ApiController
    {
        readonly IOrderRepository _orderRepository;

        public OrderController(IOrderRepository repo)
        {
            _orderRepository = repo;
        }
        // GET: api/Order
        public IHttpActionResult Get()
        {
            var a= _orderRepository.GetAll();
            return this.Ok(a);
        }

        // GET: api/Order/5
        public IHttpActionResult Get(int id)
        {
            //OrderDTO order = _orderRepository.GetAll().Single(s => s.Id == id);
            OrderDTO order = _orderRepository.Find(id);
            if (order == null)
            {
                return NotFound();
            }
            return this.Ok(order);
        }
        public void Post([FromBody]OrderDTO value)
        {
            _orderRepository.Add(value);
        }
    }
}
