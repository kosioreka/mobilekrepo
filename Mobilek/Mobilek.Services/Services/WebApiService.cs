﻿using System;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Configuration;

namespace Mobilek.Services
{

    public abstract class WebApiService
    {
        //development
        private const string address = "https://localhost:44300/api";
        //when on the same server as api
        //private const string address = "https://localhost:44302/api";
        //Mini server IP
        ////private const string address = "https://192.168.137.29:44302/api";



        public readonly HttpClient _httpClient;
        private readonly MediaTypeFormatter _formatter;

        protected WebApiService(string controllerName)
        {
            _httpClient = HttpClientFactory.Create();
            _formatter = new JsonMediaTypeFormatter();
            _httpClient.BaseAddress =
                new Uri(string.Format("{0}/{1}/", address, controllerName));
        }


        protected async Task<TResponse> Delete<TRequest, TResponse>(string uri)
        {
            return await ExtractResponse<TResponse>(await _httpClient.DeleteAsync(uri));
        }

        protected async Task<TResponse> Put<TRequest, TResponse>(string method, TRequest request)
        {
            return await ExtractResponse<TResponse>(await _httpClient.PutAsync(method, request, _formatter));
        }

        protected async Task<TResponse> Post<TRequest, TResponse>(string method, TRequest request)
        {
            return await ExtractResponse<TResponse>(await _httpClient.PostAsync(method, request, _formatter));
        }

        protected async Task<TResponse> Get<TResponse>(string uri)
        {
            return await ExtractResponse<TResponse>(await _httpClient.GetAsync(uri));
        }

        private async Task<TResponse> ExtractResponse<TResponse>(HttpResponseMessage response)
        {
            await VerifyStatusCode(response);
            return await response.Content.ReadAsAsync<TResponse>(new[] { _formatter });
        }


        private async Task VerifyStatusCode(HttpResponseMessage response)
        {
            //if (!response.IsSuccessStatusCode)
            //{
            //    TODO: Dedicated exception type
            //    throw new Exception(await response.Content.ReadAsStringAsync());
            //}
        }
    }
}
