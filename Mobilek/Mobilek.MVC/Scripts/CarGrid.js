﻿$(function () {

    var brands;
    var brandsHtml;
    $.ajax({
        url: '/Worker/GetFrenchCars',
        type: "GET",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (cars) {
            //var cos = JSON.stringify(cars);
            var carss = {};
            for (var i = 0; i < cars.length; i++) {
                var current = cars[i];
                carss[current] = current;

            }

            brands = carss;
        },
        error: function () {
        }
    });


    

    $("#carGrid").jqGrid({
        url: "/Worker/GetCars",
        datatype: 'json',
        mtype: 'Get',
        colNames: ['Id', 'Brand', 'Model', 'Price', 'Colour', 'Station', 'Production date'],
        colModel: [
            { key: true, hidden: true, name: 'Id', index: 'Id', editable: true },
            {
                key: false, name: 'brand', index: 'brand', editable: true, edittype: "select", editrules: { custom: true, custom_func: textBoxValidation },
                editoptions: {
                    value: brands, dataEvents: [{
                        type: "change",
                        fn: function (e) {
                            $("#model").attr("disabled", "disabled");
                            var index = $(e.target).val();
                            var brand = $("#brand option[value='" + index + "']").text();
                            $.ajax({
                                type: "POST",
                                url: "/Worker/GetModels",
                                contentType: "application/json; charset=utf-8",
                                dataType: "html",
                                data: JSON.stringify({ brand: brand }),
                                success: function (models) {
                                    $("#model").removeAttr("disabled").html(models);
                                },
                                error: function () {
                                    alert("er");
                                }

                            });
                        }
                    }]
                },
            },
            { key: false, name: 'model', index: 'model', editable: true, edittype: "select", editoptions: { value: { "Pick a brand": "Pick a brand" } }, editrules: { custom: true, custom_func: textBoxValidation } },
            { key: false, name: 'price', index: 'price', editable: true, edittype: "text", editrules: { custom: true, custom_func: numericBoxValidation } },
            { key: false, name: 'colour', index: 'colour', editable: true, edittype: "select", editoptions: { value: { "Pick colour": "Pick colour" } }, editrules: { custom: true, custom_func: textBoxValidation } },
            {
                key: false, name: 'stationName', index: 'stationName', editable: true, edittype: "select", editrules: { custom: true, custom_func: textBoxValidation },
                editoptions: {
                    dataUrl: "/Worker/GetStationCars", buildSelect: function (data) {
                        var response = jQuery.parseJSON(data);
                        var s = '<select>';
                        if (response && response.length) {
                            for (var i = 0, l = response.length; i < l ; i++) {
                                var ri = response[i];
                                s += '<option value="' + ri + '">' + ri + '</option>';
                            }
                        }
                        return s + "</select>";
                    }
                }
            },
            { key: false, name: 'productionDate', index: 'productionDate', editable: true, formatter: 'date', formatoptions: { newformat: 'd/m/Y' }, editrules: { custom: true, custom_func: dateBoxValidation } }],
        pager: jQuery('#pager'),
        rowNum: 10,
        rowList: [10, 20, 30, 40],
        height: '100%',
        viewrecords: true,
        caption: 'Cars',
        loadonce:true,
        emptyrecords: 'No records to display',
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
            Id: "0"
        },
        autowidth: true,
        multiselect: false
    }).navGrid('#pager', { edit: true, add: true, del: false, search: true, refresh: true },
        {
            // edit options
            zIndex: 100,
            url: '/Worker/EditCar',
            closeOnEscape: true,
            afterShowForm: function () {

                $("#brand").removeAttr("disabled").html(brandsHtml);

                $("#colour").html("<option class=\"white\" value=\"white\">WHITE</option><option class=\"red\" value=\"red\">RED</option><option class=\"yellow\" value=\"yellow\">YELLOW</option><option class=\"green\" value=\"green\">GREEN</option>");

                $("#model").attr("disabled", "disabled");
                var index = $("#brand").val();
                var brand = $("#brand option[value='" + index + "']").text();
                $.ajax({
                    type: "POST",
                    url: "/Worker/GetModels",
                    contentType: "application/json; charset=utf-8",
                    dataType: "html",
                    data: JSON.stringify({ brand: brand }),
                    success: function (models) {
                        $("#model").removeAttr("disabled").html(models);

                    },
                    error: function () {
                        alert("er");
                    }

                });
            },
            closeAfterEdit: true,
            recreateForm: true,
            afterComplete: function (response) {
                if (response.responseText) {
                    alert(response.responseText);
                    $("#carGrid").jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');

                }
            }
        },
        {
            // add options
            zIndex: 100,
            url: "/Worker/CreateCar",
            closeOnEscape: true,
            afterShowForm: function () {
                $("#brand").removeAttr("disabled").html(brandsHtml);
                $("#colour").html("<option class=\"white\" value=\"white\">WHITE</option><option class=\"red\" value=\"red\">RED</option><option class=\"yellow\" value=\"yellow\">YELLOW</option><option class=\"green\" value=\"green\">GREEN</option>");
                $("#model").attr("disabled", "disabled");
                var index = $("#brand").val();
                var brand = $("#brand option[value='" + index + "']").text();
                $.ajax({
                    type: "POST",
                    url: "/Worker/GetModels",
                    contentType: "application/json; charset=utf-8",
                    dataType: "html",
                    data: JSON.stringify({ brand: brand }),
                    success: function (models) {
                        $("#model").removeAttr("disabled").html(models);

                    },
                    error: function () {
                        alert("er");
                    }

                });


            },
            closeAfterAdd: true,
            afterComplete: function (response) {
                if (response.responseText) {
                    alert(response.responseText);
                    $("#carGrid").jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');

                }
            }
        },
        {
            // delete options
            zIndex: 100,
            url: "/Worker/DeleteCar",
            closeOnEscape: true,
            closeAfterDelete: true,
            recreateForm: true,
            msg: "Are you sure you want to delete this task?",
            afterComplete: function (response) {
                if (response.responseText) {
                    alert(response.responseText);
                    $("#carGrid").jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');

                }
            }
        },
        {
            closeOnEscape: true,
            multipleSearch: true,
            closeAfterSearch: true
        }


        );
    function textBoxValidation(value) {
        if (value == "") {
            return [false, "Field cannot be empty"];
        }
        return [true];
    }
    function numericBoxValidation(value) {
        if (value == "") {
            return [false, "Field cannot be empty"];
        }
        else if (isNaN(value)) {
            return [false, "Must be a number."];
        }
        return [true];
    }
    function zipCodeValidation(value) {
        if (value == "") {
            return [false, "Field cannot be empty"];
        }
        var re = /^\d{5}$/;
        if (!re.test(value)) {
            return [false, "Incorret Zip Code format."];
        }
        return [true];
    }

    function dateBoxValidation(value) {
        if (value == "") {
            return [false, "Field cannot be empty"];
        }
        if (value.match(/^\d{2}([./-])\d{2}\1\d{4}$/)) {
            return [true];
        } else {
            return [false, "Incorret Date format(Required DD-MM-YYYY)."];
        }

    }
});
