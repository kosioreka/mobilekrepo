﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Mobilek.DTO;
using Mobilek.WebApi.Controllers;
using System.Web.Http;
using System.Web.Http.Results;
using Mobilek.Domain;
using AutoMapper;

namespace Mobilek.WebApi.Tests
{
    [TestClass]
    public class OrderControllerTest
    {
        [TestInitialize]
        public void testInit()
        {
            Mobilek.WebApi.AutomapperConfigure.Configure();
        }
        [TestMethod]
        public void GetAllOrders_Test()
        {

            var mockRepo = new Mock<IOrderRepository>();
            mockRepo.Setup(s => s.GetAll()).Returns(new List<OrderDTO>() { new OrderDTO() });

            var controller = new OrderController(mockRepo.Object);

            var getallResult = controller.Get();

            Assert.IsInstanceOfType(getallResult, typeof(OkNegotiatedContentResult<IEnumerable<OrderDTO>>));

            var orders = getallResult as OkNegotiatedContentResult<IEnumerable<OrderDTO>>;
            Assert.IsTrue(orders.Content.Count() == 1);

        }
        [TestMethod]
        public void FindOrder_Test()
        {

            var mockRepo = new Mock<IOrderRepository>();
            int id = 5;
            int noid = 33;

            mockRepo.Setup(s => s.Find(5)).Returns(new OrderDTO() { Id = id });

            var controller = new OrderController(mockRepo.Object);

            var getallResult = controller.Get(id);

            Assert.IsInstanceOfType(getallResult, typeof(OkNegotiatedContentResult<OrderDTO>));

            var worker = getallResult as OkNegotiatedContentResult<OrderDTO>;

            Assert.IsTrue(worker.Content.Id == id);

            var getresults2 = controller.Get(noid);
            Assert.IsInstanceOfType(getresults2, typeof(NotFoundResult));

        }
    }
}
