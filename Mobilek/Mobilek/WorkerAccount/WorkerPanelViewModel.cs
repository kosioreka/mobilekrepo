﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mobilek.Helper_Classes;
using System.Windows.Input;
using System.Windows;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;
using GalaSoft.MvvmLight.Messaging;
using Mobilek.DTO;
using Mobilek.Services;

namespace Mobilek
{
    /// <summary>
    /// The <c>WorkerPanelViewModel</c> defines view for worker administration panel
    /// </summary>
    /// <remarks>
    /// Here stations, cars and being by the worker managed
    /// </remarks>
    class WorkerPanelViewModel : ObservableObject, IPageViewModel
    {
        #region private fields
        private string _logInInfo;
        private string _searchStreet;
        private string _searchStreetNo;
        private string _searchCity;
        private string _searchZipCode;
        private string _searchColour;
        private string _searchBrand;
        private string _searchModel;
        private DateTime _searchProductionDate;
        private decimal _searchPrice;
        private bool _showUnavailable;
        private WorkerDTO w;

        private ICommand _toLogOut;
        private ICommand _getCars;
        private ICommand _editCar;
        private ICommand _addCar;
        private ICommand _getStations;
        private ICommand _deleteStation;
        private ICommand _addStation;
        private ICommand _editStation;
        private ICommand _viewDetailsCarCommand;
        ObservableCollection<StationDTO> _stationsCollection = new ObservableCollection<StationDTO>();
        ObservableCollection<CarDTO> _carCollection = new ObservableCollection<CarDTO>();
        #endregion

        #region properties
        public string Name
        {
            get
            {
                return "WorkerPanel";
            }
        }
        public StationDTO SelectedStation { get; set; }
        public CarDTO SelectedCar { get; set; }
        public string SearchStreet
        {
            get { return _searchStreet; }
            set
            {
                _searchStreet = value;
                OnPropertyChanged("SearchStreet");
            }
        }
        public string SearchStreetNo
        {
            get { return _searchStreetNo; }
            set
            {
                _searchStreetNo = value;
                OnPropertyChanged("SearchStreetNo");
            }
        }
        public string SearchCity
        {
            get { return _searchCity; }
            set
            {
                _searchCity = value;
                OnPropertyChanged("SearchCity");
            }
        }
        public string SearchZipCode
        {
            get { return _searchZipCode; }
            set
            {
                _searchZipCode = value;
                OnPropertyChanged("SearchZipCode");
            }
        }
        public string SearchColour
        {
            get { return _searchColour; }
            set
            {
                _searchColour = value;
                OnPropertyChanged("SearchColour");
            }
        }
        public string SearchBrand
            {
            get { return _searchBrand; }
            set
            {
                _searchBrand = value;
                OnPropertyChanged("SearchBrand");
            }
        }
        public string SearchModel { get { return _searchModel; } set { _searchModel = value; OnPropertyChanged("SearchModel"); } }
        public DateTime SearchProductionDate { get { return _searchProductionDate; } set { _searchProductionDate = value; OnPropertyChanged("SearchProductionDate"); } }
        public decimal SearchPrice { get { return _searchPrice; } set { _searchPrice = value; OnPropertyChanged("SearchPrice"); } }
        public bool ShowUnavailable { get { return _showUnavailable; } set { _showUnavailable = value; OnPropertyChanged("ShowUnavailable"); } }
       
        public String LogInInfo
        {
            get { return _logInInfo; }
            set
            {
                _logInInfo = value;
                OnPropertyChanged("logInInfo");
            }
        }
        public ObservableCollection<StationDTO> StationsCollection
        {
            get
            {
                return _stationsCollection;
            }
            set
            {
                _stationsCollection = value;
                OnPropertyChanged("StationsCollection");
            }
        }
        public ObservableCollection<CarDTO> CarsCollection
        {
            get
            {
                return _carCollection;
            }
            set
            {
                _carCollection = value;
                OnPropertyChanged("CarsCollection");
            }
            
        }
        #endregion

        #region Commands
        public ICommand GetStationsCommand
        {
            get
            {
                if (_getStations == null)
                {
                    _getStations = new RelayCommand(
                        param => GetStations()
                    );
                }
                return _getStations;
            }
        }
        public ICommand DeleteStationCommand
        {
            get
            {
                if (_deleteStation == null)
                {
                    _deleteStation = new RelayCommand(
                        param => DeleteStation()
                    );
                }
                return _deleteStation;
            }
        }
        public ICommand EditStationCommand
        {
            get
            {
                if (_editStation == null)
                {
                    _editStation = new RelayCommand(
                        param => EditStation()
                    );
                }
                return _editStation;
            }
        }
        public ICommand AddStationCommand
        {
            get
            {
                if (_addStation == null)
                {
                    _addStation = new RelayCommand(
                        param => AddStation()
                    );
                }
                return _addStation;
            }
        }

        public ICommand GetCarsCommand
        {
            get
            {
                if (_getCars == null)
                {
                    _getCars = new RelayCommand(
                        param => GetCars()
                    );
                }
                return _getCars;
            }
        }
        public ICommand AddCarCommand
        {
            get
            {
                if (_addCar == null)
                {
                    _addCar = new RelayCommand(
                        param => AddCar()
                        );

                }
                return _addCar;
            }

        }
        public ICommand EditCarCommand
        {
            get
            {
                if (_editCar == null)
                {
                    _editCar = new RelayCommand(
                        param => EditCar()
                        );

                }
                return _editCar;
            }
        }
        public ICommand ToLogOut
        {
            get
            {
                if (_toLogOut == null)
                {
                    _toLogOut = new RelayCommand(
                        param => ToWelcomeView()
                    );
                }
                return _toLogOut;
        }
        }
        public ICommand ViewDetailsCarCommand
        {
            get
            {
                if (_viewDetailsCarCommand == null)
                {
                    _viewDetailsCarCommand = new RelayCommand(
                        param => toViewDetailsCar()
                        );
                }
                return _viewDetailsCarCommand;
            }
        }
        #endregion

        #region Methods
        public WorkerPanelViewModel(WorkerDTO w)
        {
            Messenger.Default.Register<FireRefresh>
            (
                 this,
                 (action) => Refresh()
            );
            _logInInfo = w.login.ToString();
            this.w = w;

            SearchProductionDate = new DateTime(2000, 1, 1);
            SearchPrice = 10000;
        }
        private void Refresh()
        {
            GetStations();
            GetCars();
        }
        private async void DeleteStation()
        {
            if (SelectedStation != null)
            {                  
                var service = new StationDTOService(w.login,w.password);
                var toRemove = await service.Get(SelectedStation.Id);

                await service.Delete(toRemove.Id);
                GetStations();
            }
        }
        private async void GetStations()
        {
            var service = new StationDTOService(w.login, w.password);
            var stationss = await service.Get();
            var stations = stationss.AsEnumerable();

                if (!string.IsNullOrWhiteSpace(_searchStreet))
                {
                    stations = stations.Where(s => s.street.StartsWith(_searchStreet));
                }
                if (!string.IsNullOrWhiteSpace(_searchStreetNo))
                {
                    int no;
                    if (Int32.TryParse(_searchStreetNo, out no))
                    {
                        stations = stations.Where(s => s.streetNumer == no);
                    }
                }
                if (!string.IsNullOrWhiteSpace(_searchCity))
                {
                    stations = stations.Where(s => s.city.StartsWith(_searchCity));
                }
                if (!string.IsNullOrWhiteSpace(_searchZipCode))
                {
                    stations = stations.Where(s => s.zipCode.StartsWith(_searchZipCode));
                }
            StationsCollection = ToObservableCollectioncs.ToObservableCollection<StationDTO>(stations);
        }
        private async void GetCars()
            {
            var service = new CarDTOService(w.login, w.password);
            var carss = await service.Get();
            var cars = carss.AsEnumerable();
            
                if (!string.IsNullOrWhiteSpace(SearchModel))
                {
                    cars = cars.Where(s => s.model.StartsWith(SearchModel));
                }
                if (!string.IsNullOrWhiteSpace(SearchBrand))
                {
                    cars = cars.Where(s => s.brand.StartsWith(SearchBrand));
                }
                if (!string.IsNullOrWhiteSpace(SearchColour))
                {
                    cars = cars.Where(s => s.colour.StartsWith(SearchColour));
                }
                cars = cars.Where(s => s.productionDate > SearchProductionDate);

            if (!ShowUnavailable)
                {
                    cars = cars.Where(s => s.isEfficient == true);
                }
                
                cars = cars.Where(s => s.price < SearchPrice);
            CarsCollection = ToObservableCollectioncs.ToObservableCollection<CarDTO>(cars);

        }
        #endregion

        #region Navigation
        private async void EditStation()
        {
            if (SelectedStation != null)
            {
                CustomDialog d = new CustomDialog();
                d.Content = new StationEditView(SelectedStation,w);
                var metro = App.Current.MainWindow as MetroWindow;
                await metro.ShowMetroDialogAsync(d);
            }
        }
        private async void AddStation()
        {
            CustomDialog d = new CustomDialog();
            d.Content = new StationAddView(w);
            var metro = App.Current.MainWindow as MetroWindow;
            await metro.ShowMetroDialogAsync(d);
        }
        private async void AddCar()
        {
            CustomDialog d = new CustomDialog();
            d.Content = new CarAddView(w);
            var metro = App.Current.MainWindow as MetroWindow;
            await metro.ShowMetroDialogAsync(d);
        }
        private async void EditCar()
        {
            if (SelectedCar != null)
            {
                CustomDialog d = new CustomDialog();
                d.Content = new CarEditView(SelectedCar,w);
                var metro = App.Current.MainWindow as MetroWindow;
                await metro.ShowMetroDialogAsync(d);
            }
        }
        private async void toViewDetailsCar()
        {
            if (SelectedCar != null)
            {
                CustomDialog d = new CustomDialog();
                d.Content = new CarDetailView(SelectedCar);
                var metro = App.Current.MainWindow as MetroWindow;
                await metro.ShowMetroDialogAsync(d);
            }
        }
        private void ToWelcomeView()
        {
            var msg = new ChangePageHelper() { PageName = new WelcomeViewModel() };
            Messenger.Default.Send<ChangePageHelper>(msg);
        }
        #endregion
    }
}
