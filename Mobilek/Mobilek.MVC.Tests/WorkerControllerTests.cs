﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mobilek.DTO;
using Mobilek.MVC.Constants;
using Mobilek.MVC.Controllers;
using Mobilek.Services;
using Moq;

namespace Mobilek.MVC.Tests
{
    [TestClass]
    public class WorkerControllerTests
    {
        [TestMethod]
        public void GetCarGridTest()
        {
            var serviceMock = new Mock<ICarDTOService>();
            var serviceMock2 = new Mock<IStationDTOService>();

            List<CarDTO> l=new List<CarDTO>()
            {
                new CarDTO()
            };


            serviceMock.Setup(s => s.Get()).ReturnsAsync(l);

            WorkerController controller=new WorkerController(serviceMock.Object,serviceMock2.Object);

            var cars =controller.GetCars("", "", 1, 1);

            Assert.IsInstanceOfType(cars,typeof(JsonResult));
        }


        [TestMethod]
        public void GetStationCarsTest()
        {
            var serviceMock = new Mock<ICarDTOService>();
            var serviceMock2 = new Mock<IStationDTOService>();

            
            List<StationDTO> l=new List<StationDTO>();

            serviceMock2.Setup(s => s.Get()).ReturnsAsync(l);

            WorkerController controller = new WorkerController(serviceMock.Object, serviceMock2.Object);

            var stations = controller.GetStationCars();    

            Assert.IsInstanceOfType(stations, typeof(JsonResult));
        }
        [TestMethod]
        public void WorkerIndexNaiveTest()
        {

            var serviceMock = new Mock<ICarDTOService>();
            var serviceMock2 = new Mock<IStationDTOService>();

            WorkerController controller = new WorkerController(serviceMock.Object, serviceMock2.Object);

            var result = controller.Index();

            Assert.IsInstanceOfType(result,typeof(ViewResult));

        }

        [TestMethod]
        public void WorkerCreateCarValidationFailTest()
        {
            var serviceMock = new Mock<ICarDTOService>();
            var serviceMock2 = new Mock<IStationDTOService>();

            WorkerController controller = new WorkerController(serviceMock.Object, serviceMock2.Object);

            var result = controller.CreateCar(new CarDTO());

            Assert.IsTrue(result.Contains(Messages.ErrorOccured));
        }
        [TestMethod]
        public void GetFrenchCarTest()
        {
            var serviceMock = new Mock<ICarDTOService>();
            var serviceMock2 = new Mock<IStationDTOService>();

            WorkerController controller = new WorkerController(serviceMock.Object, serviceMock2.Object);

            var result = controller.GetFrenchCars();

            Assert.IsInstanceOfType(result,typeof(JsonResult));
        }

        [TestMethod]
        public void EditCarTest()
        {
            var serviceMock = new Mock<ICarDTOService>();
            var serviceMock2 = new Mock<IStationDTOService>();

            WorkerController controller = new WorkerController(serviceMock.Object, serviceMock2.Object);

            var result = controller.EditCar(new CarDTO());

            Assert.IsTrue(result.Contains(Messages.ErrorOccured));
        }

        [TestMethod]
        public void GetStationGridTest()
        {
            var serviceMock = new Mock<ICarDTOService>();
            var serviceMock2 = new Mock<IStationDTOService>();

            List<StationDTO> l = new List<StationDTO>()
            {
                new StationDTO()
            };


            serviceMock2.Setup(s => s.Get()).ReturnsAsync(l);

            WorkerController controller = new WorkerController(serviceMock.Object, serviceMock2.Object);

            var cars = controller.GetStations("", "", 1, 1);

            Assert.IsInstanceOfType(cars, typeof(JsonResult));
        }


    }
}
