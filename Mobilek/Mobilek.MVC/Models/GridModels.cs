﻿using Mobilek.DTO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Mobilek.MVC.Models
{
    public class GridModels
    {
        public IEnumerable<OrderDTO> OrdersCollection { get; set; }
        public CustomerDTO TheCustomer;
        public async Task<List<OrderDTO>> GetOrders(int? page, int? limit, string sortBy, string direction, string searchString)//, out int total)
        {
            
                var service = new Mobilek.Services.OrderDTOService(TheCustomer.userName, TheCustomer.password);
                var orders = await service.Get();
                var orderList = orders.Where(o => o.Customer.Id.Equals(TheCustomer.Id));
                //var orderList = entities.Orders.AsEnumerable().Where(o => o.Customer.Id.Equals(_theCustomer.Id));
                OrdersCollection = orderList;
            
            //var doc = XDocument.Load(HttpContext.Current.Server.MapPath("~/App_Data/Players.xml"));
            //total = doc.Descendants("Player").Count();
            //var records = (from p in doc.Descendants("Player")
            //               select new Player
            //               {
            //                   ID = int.Parse(p.Element("ID").Value),
            //                   Name = p.Element("Name").Value,
            //                   PlaceOfBirth = p.Element("PlaceOfBirth").Value,
            //                   DateOfBirth = p.Element("DateOfBirth").Value
            //               }).AsQueryable();

            //if (!string.IsNullOrWhiteSpace(searchString))
            //{
            //    records = records.Where(p => p.Name.Contains(searchString) || p.PlaceOfBirth.Contains(searchString));
            //}

            //if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            //{
            //    if (direction.Trim().ToLower() == "asc")
            //    {
            //        records = SortHelper.OrderBy(records, sortBy);
            //    }
            //    else
            //    {
            //        records = SortHelper.OrderByDescending(records, sortBy);
            //    }
            //}

            //if (page.HasValue && limit.HasValue)
            //{
            //    int start = (page.Value - 1) * limit.Value;
            //    records = records.Skip(start).Take(limit.Value);
            //}
            //total = 0;
            //return records.ToList();
            return orderList.ToList();
        }
    }
    public class OrderModel
    {
        public OrderDTO order { get; set; }
        public List<CarDTO> cars { get; set; }
    }
    public class StationModel
    {

    }
}