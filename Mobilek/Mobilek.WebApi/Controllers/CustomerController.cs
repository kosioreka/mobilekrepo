﻿using Mobilek.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Mobilek.WebApi.Controllers
{
    

    public class CustomerController : ApiController
    {
        readonly ICustomerRepository _customerRepository;

        public CustomerController(ICustomerRepository repo)
        {
            _customerRepository = repo;
        }
        [Authorize]
        public IHttpActionResult Get()
        {
            return this.Ok(_customerRepository.GetAll());
        }
        [Authorize]
        // GET: api/Station/5
        public IHttpActionResult Get(string id)
        {
            //CustomerDTO cust = _customerRepository.GetAll().Single(s => s.Id == id);
            CustomerDTO cust = _customerRepository.Find(id);
            //if (cust == null)
            //{
            //    return NotFound();
            //}
            return this.Ok(cust);

        }

        // POST: api/Station
        public IHttpActionResult Post([FromBody]CustomerDTO value)
        {
            _customerRepository.Add(value);
            return this.Ok((true));
        }

        // PUT: api/Station/
        [Authorize]
        public IHttpActionResult Put([FromBody]CustomerDTO value)
        {
            _customerRepository.Update(value);
            return this.Ok(true);
        }

    }
}
