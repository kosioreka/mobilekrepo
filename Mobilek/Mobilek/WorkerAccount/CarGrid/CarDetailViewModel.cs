﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Mobilek.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Mobilek
{
    class CarDetailViewModel: ObservableObject
    {
        private CarDTO car;
        private ICommand _goBack;
        private CarFromApi frenchCar;
        public CarDetailViewModel(CarDTO c)
        {
            car = c;
            frenchCar = FrenchApi.GetCarById(c.frenchId);

        }
        public CarFromApi FrenchCar
        {
            get { return frenchCar; }
            set
            {
                frenchCar = value;
                OnPropertyChanged("FrenchCar");
            }
        } 
        public ICommand GoBack
        {
            get
            {
                if (_goBack == null)
                {
                    _goBack = new RelayCommand(
                        param => Exit());

                }
                return _goBack;
            }
        }
        private async void Exit()
        {
            var metroWindow = App.Current.MainWindow as MetroWindow;
            var actualDialog = await metroWindow.GetCurrentDialogAsync<CustomDialog>();
            await metroWindow.HideMetroDialogAsync(actualDialog);
        }
    }
}
