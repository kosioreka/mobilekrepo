﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;

namespace Mobilek.MVC.Helpers
{
    public class FrenchApi
    {
        public static List<string> GetUniqueBrandCars()
        {
            string url = "http://public.opendatasoft.com/api/odata/vehicules-commercialises?$select=marque,modele_dossier";
            var request = WebRequest.Create(url);
            request.ContentType = "application/json; charset=utf-8";
            string text;
            var response = (HttpWebResponse)request.GetResponse();

            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                text = sr.ReadToEnd();
            }
            var des = JsonConvert.DeserializeObject<FrenchResponse>(text);

            //var ret= des.records.Select(s => s.fields.marque).Distinct().ToList();
            var ret = des.value.Select(s => s.marque).Distinct().ToList();

            return ret;
        }
        public static async Task<List<string>> GetUniqueBrandCarsAsync()
        {
            string url = "http://public.opendatasoft.com/api/odata/vehicules-commercialises?$select=marque,modele_dossier";
            var request = WebRequest.Create(url);
            request.ContentType = "application/json; charset=utf-8";
            string text;
            //var response = await (HttpWebResponse)request.GetResponse();
            var response = await request.GetResponseAsync();

            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                text = sr.ReadToEnd();
            }
            var des = JsonConvert.DeserializeObject<FrenchResponse>(text);
            var ret = des.value.Select(s => s.marque).Distinct().ToList();

            return ret;
        }
        public static List<CarFromApi> GetCarsByBrand(string marque)
        {
            //string url = "http://data.opendatasoft.com/api/records/1.0/search?dataset=vehicules-commercialises%40public&rows=10000&sort=puissance_maximale"
            //    + "&refine.marque=" + marque;
            string url = "http://public.opendatasoft.com/api/odata/vehicules-commercialises?$select=marque,modele_dossier,recordid&$filter=marque eq '" + marque + "'";
            var request = WebRequest.Create(url);
            request.ContentType = "application/json; charset=utf-8";
            string text;
            var response = (HttpWebResponse)request.GetResponse();

            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                text = sr.ReadToEnd();
            }
            var des = JsonConvert.DeserializeObject<FrenchResponse>(text);

            //var ret = des.records.Select(s => s.fields.modele_dossier).Distinct().ToList();
            var ret = des.value.Select(s => s.modele_dossier).Distinct().ToList();
            var rre = des.value.GroupBy(s => s.modele_dossier).Select(g => g.First()).ToList();
            var a = Mapper.Map<List<CarDetails>, List<CarFromApi>>(rre);
            return a;
        }
        public static CarFromApi GetCarById(string ID)
        {
            string url = "http://public.opendatasoft.com/api/odata/vehicules-commercialises" + "('" + ID + "')";
            var request = WebRequest.Create(url);
            request.ContentType = "application/json; charset=utf-8";
            string text;
            var response = (HttpWebResponse)request.GetResponse();
            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                text = sr.ReadToEnd();
            }

            var des = JsonConvert.DeserializeObject<CarDetails>(text);
            //var ret = des.records.Where(r => r.recordid == ID).FirstOrDefault();
            var mapped = Mapper.Map<CarDetails, CarFromApi>(des);
            return mapped;
        }

        public static CarFromApi GetCarByBrandAndModel(string brand, string model)
        {
            var models = FrenchApi.GetCarsByBrand(brand);
            var modelCar = models.FirstOrDefault(s => s.modele_dossier == model);
            return modelCar;
        }


    }
    public class FrenchResponse
    {
        public List<CarRecord> records { get; set; }
        public List<CarDetails> value { get; set; }
    }
    public class CarRecord
    {
        public string datasetid { get; set; }
        public string recordid { get; set; }
        public CarDetails fields { get; set; }

    }
    public class CarDetails
    {
        public string recordid { get; set; }
        public string marque { get; set; }
        public string modele_dossier { get; set; }
        public int annee { get; set; }
        public decimal hc_g_km { get; set; }
        public string consommation_extra_urbaine_l_100km { get; set; }
        public string masse_vide_euro_min_kg { get; set; }
        public string puissance_maximale { get; set; }
        public string carrosserie { get; set; }
        public string co_type_i_g_km { get; set; }
        public string hybride { get; set; }
        public double co2_g_km { get; set; }
        public decimal consommation_mixte_l_100km { get; set; }

    }

    public class CarFromApi
    {
        public string recordid { get; set; }
        public string marque { get; set; }
        public string modele_dossier { get; set; }
        public int annee { get; set; }
        public decimal hc_g_km { get; set; }
        public string consommation_extra_urbaine_l_100km { get; set; }
        public string masse_vide_euro_min_kg { get; set; }
        public string puissance_maximale { get; set; }
        public string carrosserie { get; set; }
        public string co_type_i_g_km { get; set; }
        public string hybride { get; set; }
        public double co2_g_km { get; set; }
        public decimal consommation_mixte_l_100km { get; set; }
    }
}
