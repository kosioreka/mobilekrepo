﻿using GalaSoft.MvvmLight.Messaging;
using Mobilek.DTO;
using Mobilek.Helper_Classes;
using Mobilek.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace Mobilek
{
    /// <summary>
    /// Class defining user's login and registation process
    /// </summary>
    public class UserLoginViewModel : ObservableObject, IPageViewModel, IDataErrorInfo
    {

        #region private Fields 

        private ICommand _toCustomerAccount;
        private ICommand _toCustomerLogin;
        private ICommand _toLogOut;
        private string registrationSuccessful;

        private string login_userName;
        private string login_password;

        private string firstName;
        private string surname;
        private string pesel;
        private string creditCardNr;
        private string userName;
        private string phoneNr;
        private string email;
        private string password;
        private string repeatPassword;
        public Dictionary<string, string> validationErrors = new Dictionary<string, string>();

        CustomerDTOService service;
        #endregion

        #region properties
        public string Name
        {
            get { return "Log in my friend"; }
        }
        public string RegistrationSuccessful
        {
            get { return registrationSuccessful; }
            set
            {
                registrationSuccessful = value;
                OnPropertyChanged("RegistrationSuccessful");
            }
        }
        public string Login_UserName
        {
            get { return login_userName; }
            set
            {
                login_userName = value;
                OnPropertyChanged("Login_UserName");

            }
        }
        public string Login_Password
        {
            get { return login_password; }
            set
            {
                login_password = value;
                OnPropertyChanged("Login_Password");

            }
        }
        public string FirstName
        {
            get { return firstName; }
            set
            {
                firstName = value;
                OnPropertyChanged("FirstName");

            }
        }
        public string Surname
        {
            get { return surname; }
            set
            {
                surname = value;
                OnPropertyChanged("Surname");

            }
        }
        public string Pesel
        {
            get { return pesel; }
            set
            {
                pesel = value;
                OnPropertyChanged("Pesel");

            }
        }
        public string CreditCardNr
        {
            get { return creditCardNr; }
            set
            {
                creditCardNr = value;
                OnPropertyChanged("CreditCardNr");

            }
        }
        public string UserName
        {
            get { return userName; }
            set
            {
                userName = value;
                OnPropertyChanged("UserName");

            }
        }
        public string PhoneNr
        {
            get { return phoneNr; }
            set
            {
                phoneNr = value;
                OnPropertyChanged("PhoneNr");

            }
        }
        public string Email
        {
            get { return email; }
            set
            {
                email = value;
                OnPropertyChanged("Email");

            }
        }
        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                OnPropertyChanged("Password");

            }
        }
        public string RepeatPassword
        {
            get { return repeatPassword; }
            set
            {
                repeatPassword = value;
                OnPropertyChanged("RepeatPassword");

            }
        }
        #endregion

        #region Validation

        public void ValidateLogin()
        {
            service = new CustomerDTOService(Login_UserName, Login_Password);
            validationErrors.Clear();
            try
            {
                if (String.IsNullOrWhiteSpace(Login_UserName))
                {
                    validationErrors.Add("Login_UserName", "Please enter your user name");
                }
                {
                    //    var service = new Mobilek.Services.CustomerDTOService();
                    //    var users = Task.Run(() => service.Get()).Result;
                    //    var user = users.FirstOrDefault(x => x.userName == Login_UserName);
                    var user = Task.Run(() => service.Get(Login_UserName)).Result;
                    if (user == null)
                    {
                        if (!validationErrors.ContainsKey("Login_UserName"))
                        {
                            validationErrors.Add("Login_UserName", "User not found");
                        }
                    }
                }
                {
                    //var service = new Mobilek.Services.CustomerDTOService();
                    //var users = Task.Run(()=> service.Get()).Result;
                    //var user = users.FirstOrDefault(x => x.userName == Login_UserName);
                    var user = Task.Run(() => service.Get(Login_UserName)).Result;
                    if (user == null)
                    {
                        if (!validationErrors.ContainsKey("Login_UserName"))
                        {
                            validationErrors.Add("Login_UserName", "User not found");
                        }
                    }
                    else
                    {
                        var result = SecurePasswordHasher.Verify(Login_Password, user.password);
                        if (!result)
                        {
                            if (!validationErrors.ContainsKey("Login_UserName"))
                            {
                                validationErrors.Add("Login_UserName", "Incorrect password");
                            }
                        }
                    }
                }
            }
            catch
            {
                validationErrors.Add("Login_UserName", "User not found");
            }
            OnPropertyChanged(null);
        }


        public void ValidateRegistry()
        {
            service = new CustomerDTOService(Login_UserName, Login_Password);
            validationErrors.Clear();
            if (String.IsNullOrWhiteSpace(FirstName))
            {
                validationErrors.Add("FirstName", "First name is required!");
            }
            if (String.IsNullOrWhiteSpace(Surname))
            {
                validationErrors.Add("Surname", "Surname is required!");
            }
            if (String.IsNullOrEmpty(Pesel))
            {
                validationErrors.Add("Pesel", "Pesel is required!");
            }
            else if (Regex.IsMatch(Pesel, @"^\d+$") == false)
                validationErrors.Add("Pesel", "Only digits are allowed in Pesel field.");
            else if (Pesel.Length != 11)
                validationErrors.Add("Pesel", "Wrong Pesel Number!");
            if (String.IsNullOrEmpty(UserName))
            {
                validationErrors.Add("UserName", "User name is required!");
            }
            else
            {
                //var service = new Mobilek.Services.CustomerDTOService();
                //var users = Task.Run(() =>  service.Get()).Result;
                //var user = users.FirstOrDefault(x => x.userName == UserName);
                try
                {
                    var user = Task.Run(() => service.Get(Login_UserName)).Result;
                    if (user != null)
                    {
                        validationErrors.Add("UserName", "User name is not available. Please choose different one");
                    }
                }
                catch
                {

                }

            }
            if (String.IsNullOrEmpty(PhoneNr))
            {
                validationErrors.Add("PhoneNr", "Phone is required!");
            }
            else if (Regex.IsMatch(PhoneNr, @"^\d+$") == false)
                validationErrors.Add("PhoneNr", "Only digits are allowed in Phone Number field.");
            if (String.IsNullOrEmpty(Email))
            {
                validationErrors.Add("Email", "Email is required!");
            }
            else if (Regex.IsMatch(Email, @"^[A-Za-z0-9_\-\.]+@(([A-Za-z0-9\-])+\.)+([A-Za-z\-])+$") == false)
            {
                validationErrors.Add("Email", "Please enter valid e-mail adress.");
            }
            else
            {
                //var service = new Mobilek.Services.CustomerDTOService();
                //     var users = Task.Run(() => service.Get()).Result;
                //   var user = users.FirstOrDefault(x => x.email == Email);
                try
                {
                    var user = Task.Run(() => service.Get(Login_UserName)).Result;
                    if (user != null)
                    {
                        validationErrors.Add("Email", "This email address has already an account");
                    }
                }
                catch
                {

                }
            }
            if (String.IsNullOrEmpty(Password))
            {
                if (!validationErrors.ContainsKey("UserName"))
                {
                    validationErrors.Add("UserName", "Password is required!");
                }

            }
            else if (Password.Length < 8)
            {
                if (!validationErrors.ContainsKey("UserName"))
                {
                    validationErrors.Add("UserName", "Password must have at least 8 characters");
                }
            }
            OnPropertyChanged(null);
        }
        #endregion

        #region Commands
        public ICommand ToCustomerAccount
        {
            get
            {
                if (_toCustomerAccount == null)
                {
                    _toCustomerAccount = new RelayCommand(
                        param => ToAccount(param)
                    );
                }
                return _toCustomerAccount;
            }
        }
        public ICommand ToCustomerRegistration
        {
            get
            {
                if (_toCustomerLogin == null)
                {
                    _toCustomerLogin = new RelayCommand(
                        param => ToCustomer(param)
                    );
                }
                return _toCustomerLogin;
            }
        }

        public string Error
        {
            get
            {
                if (validationErrors.Count > 0)
                {
                    return "Errors found.";
                }
                return null;
            }
        }

        public string this[string columnName]
        {
            get
            {
                if (validationErrors.ContainsKey(columnName))
                {
                    return validationErrors[columnName];
                }
                return null;
            }
        }
        public ICommand ToLogOut
        {
            get
            {
                if (_toLogOut == null)
                {
                    _toLogOut = new RelayCommand(
                        param => ToWelcomeView()
                    );
                }
                return _toLogOut;
            }
        }
        #endregion

        #region NavigationMethods
        private void ToWelcomeView()
        {
            var msg = new ChangePageHelper() { PageName = new WelcomeViewModel() };
            Messenger.Default.Send<ChangePageHelper>(msg);
        }
        private async void ToAccount(object parameter)
        {
            var passwordBox = parameter as PasswordBox;
            var password = passwordBox.Password;
            Login_Password = password;
            ValidateLogin();
            if (validationErrors.Count == 0)
            {
                CustomerDTO customer = new CustomerDTO();
                var service = new Mobilek.Services.CustomerDTOService(Login_UserName, Login_Password);
                var users = await service.Get();
                customer = users.FirstOrDefault(x => x.userName == Login_UserName);

                var msg = new ChangePageHelper() { PageName = new UserPanelViewModel(customer) };
                Messenger.Default.Send<ChangePageHelper>(msg);

            }
            //var msg2 = new ChangePanelHelper() { panelType = PanelType.User };
            //Messenger.Default.Send<ChangePanelHelper>(msg2); 
        }
        private async void ToCustomer(object parameter)
        {
            var passwordBox = parameter as PasswordBox;
            var password = passwordBox.Password;
            Password = password;
            ValidateRegistry();
            if (validationErrors.Count == 0)
            {
                //using (MobilekEntities entities = new MobilekEntities())
                {
                    var hash = SecurePasswordHasher.Hash(password);
                    CustomerDTO customer = new CustomerDTO()
                    {
                        firstName = FirstName,
                        lastName = Surname,
                        creditCardNumber = CreditCardNr,
                        pesel = Pesel,
                        phoneNumber = PhoneNr,
                        userName = UserName,
                        password = hash,
                        email = Email
                    };
                    //entities.Customers.Add(customer);
                    //entities.SaveChanges();
                    var service = new Mobilek.Services.CustomerDTOService("", "");
                    await service.Post(customer);
                }
                RegistrationSuccessful = "User name: " + UserName + " was successfully added";
                FirstName = ""; Surname = ""; Email = ""; PhoneNr = ""; Pesel = ""; CreditCardNr = "";
                Password = ""; RepeatPassword = ""; UserName = ""; Login_Password = ""; Login_UserName = "";
                var msg = new ChangePageHelper() { PageName = new UserLoginViewModel() };
                Messenger.Default.Send<ChangePageHelper>(msg);
            }

        }

        #endregion
    }
}
