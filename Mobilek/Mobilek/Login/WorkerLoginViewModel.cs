﻿using GalaSoft.MvvmLight.Messaging;
using Mobilek.DTO;
using Mobilek.Helper_Classes;
using Mobilek.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace Mobilek
{
    /// <summary>
    /// Class defining 
    /// s's login process
    /// </summary>
    /// <remarks>
    /// Note that administrators cannot register like user's. It is required that admin register new admins.
    /// </remarks>
    public class WorkerLoginViewModel : ObservableObject, IPageViewModel, IDataErrorInfo
    {
        #region private fields
        private ICommand _toWorkerAccount;
        private ICommand _toLogOut;
        private string login_userName;
        private string login_password;
        #endregion

        WorkerDTOService service;

        #region properties
        public Dictionary<string, string> validationErrors = new Dictionary<string, string>();
        public string Name
        {
            get { return "Log in Admin"; }
        }

        public string Login_UserName
        {
            get { return login_userName; }
            set
            {
                login_userName = value;
                OnPropertyChanged("Login_UserName");

            }
        }
        public string Login_Password
        {
            get { return login_password; }
            set
            {
                login_password = value;
                OnPropertyChanged("Login_Password");

            }
        }
        #endregion

        #region Validation
        public string Error
        {
            get
            {
                if (validationErrors.Count > 0)
                {
                    return "Errors found.";
                }
                return null;
            }
        }

        public string this[string columnName]
        {
            get
            {
                if (validationErrors.ContainsKey(columnName))
                {
                    return validationErrors[columnName];
                }
                return null;
            }
        }
        #endregion

        #region commmands/methods
        public ICommand ToLogOut
        {
            get
            {
                if (_toLogOut == null)
                {
                    _toLogOut = new RelayCommand(
                        param => ToWelcomeView()
                    );
                }
                return _toLogOut;
            }
        }
        private void ToWelcomeView()
        {
            var msg = new ChangePageHelper() { PageName = new WelcomeViewModel() };
            Messenger.Default.Send<ChangePageHelper>(msg);
        }
        public ICommand ToWorkerAccount
        {
            get
            {
                if (_toWorkerAccount == null)
                {
                    _toWorkerAccount = new RelayCommand(
                        param => ToAccount(param)
                    );
                }
                return _toWorkerAccount;
            }
        }
        public void Validate()
        {
            service = new WorkerDTOService(Login_UserName, Login_Password);
            validationErrors.Clear();
            try
            {
                if (String.IsNullOrWhiteSpace(Login_UserName))
                {
                    validationErrors.Add("Login_UserName", "Please enter your user name");
                }
                {
                    //var service = new Mobilek.Services.WorkerDTOService();
                    //var users = Task.Run(() => service.Get()).Result;
                    //var user=users.FirstOrDefault(x => x.login == Login_UserName);
                    var user = Task.Run(() => service.Get(Login_UserName)).Result;
                    if (user == null)
                    {
                        if (!validationErrors.ContainsKey("Login_UserName"))
                        {
                            validationErrors.Add("Login_UserName", "User not found");
                        }
                    }
                }
                {
                    //var service = new Mobilek.Services.WorkerDTOService();
                    //var users = Task.Run(() => service.Get()).Result;
                    //var user = users.FirstOrDefault(x => x.login == Login_UserName);
                    var user = Task.Run(() => service.Get(Login_UserName)).Result;
                    if (user == null)
                    {
                        if (!validationErrors.ContainsKey("Login_UserName"))
                        {
                            validationErrors.Add("Login_UserName", "User not found");
                        }
                    }
                    else
                    {

                        var result = SecurePasswordHasher.Verify(Login_Password, user.password);
                        if (!result)
                        {
                            if (!validationErrors.ContainsKey("Login_UserName"))
                            {
                                validationErrors.Add("Login_UserName", "Incorrect password");
                            }

                        }
                    }
                }
            }
            catch
            {
                validationErrors.Add("Login_UserName", "User not found");
                //OnPropertyChanged(null);
            }
            OnPropertyChanged(null);
        }
        private void ToAccount(object parameter)
        {
            var passwordBox = parameter as PasswordBox;
            var password = passwordBox.Password;
            Login_Password = password;
            Validate();
            if (validationErrors.Count == 0)
            {
                WorkerDTO w;
                {
                    //var service = new Mobilek.Services.WorkerDTOService();
                    //var users = Task.Run(() => service.Get()).Result;
                    //w = users.Single(s => s.login == Login_UserName);
                    w = Task.Run(() => service.Get(Login_UserName)).Result;
                }
                var msg = new ChangePageHelper() { PageName = new WorkerPanelViewModel(w) };
                Messenger.Default.Send<ChangePageHelper>(msg);
            }

        }
        #endregion
    }
}
