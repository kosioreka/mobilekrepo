﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mobilek.DTO;
using System.Net.Http.Headers;

namespace Mobilek.Services
{
    public interface ICarDTOService
    {
        Task<CarDTO> Get(int id);

        Task<IList<CarDTO>> Get();

        Task<bool> Post(CarDTO item);

        Task<bool> Put(CarDTO item);

        Task<CarDTO> Delete(int id);

    }



    public class CarDTOService : WebApiService,ICarDTOService
    {
        public CarDTOService(string l,string p) : base("Car")
        {
            this._httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue(
                    "Basic",
                    Convert.ToBase64String(
                        System.Text.ASCIIEncoding.ASCII.GetBytes(
                            string.Format("{0}:{1}", l, p))));
        }

        public async Task<CarDTO> Get(int id)
        {
            return await this.Get<CarDTO>(id.ToString());
        }
        public async Task<IList<CarDTO>> Get()
        {
            return await this.Get<IList<CarDTO>>("");
        }
        public async Task<bool> Post(CarDTO item)
        {
            return await this.Post<CarDTO, bool>("POST", item);
        }
        public async Task<bool> Put(CarDTO item)
        {
            return await this.Put<CarDTO, bool>("PUT", item);
        }
        public async Task<CarDTO> Delete(int id)
        {
            return await this.Delete<CarDTO, CarDTO>(id.ToString());
        }

        //public async Task<IList<StudentDetailsDto>> Search(string searchPhrase)
        //{
        //    return await this.Get<IList<StudentDetailsDto>>(string.Concat("Search/", HttpUtility.UrlEncode(searchPhrase)));
        //}
    }
}
