﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobilek.DTO
{
    public class StationDTO
    {
        public int Id { get; set; }
        public string street { get; set; }
        public int streetNumer { get; set; }
        public string city { get; set; }
        public string zipCode { get; set; }

        public override string ToString()
        {
            string strOut = string.Format("{0} {1}", street, streetNumer);
            return strOut;
        }

        //public ICollection<CarDTO> Cars { get; set; }
        //public  ICollection<OrderDTO> Orders { get; set; }
    }
}
